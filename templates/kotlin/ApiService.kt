package com.appgen.app.api

import io.reactivex.Flowable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import retrofit2.http.Body
import com.appgen.app.models.*

interface ApiService {

    /* INTRON ```
    //__Model__
	
    @GET("__models__")
	fun __models__(): Flowable<ArrayList<__Model__>>

	@GET("__models__/{id}")
	fun __model__(@Path("id") id: Int): Flowable<__Model__>

	@PATCH("__models__/{id}")
	fun edit__Model__(@Path("id") id: Int, @Body __model__: __Model__): Flowable<__Model__>

	@POST("__models__")
	fun create__Model__(@Body __model__: __Model__): Flowable<__Model__>

	@DELETE("__models__/{id}")
	fun delete__Model__(@Path("id") id: Int): Flowable<Any>
    
    ``` */
    

}