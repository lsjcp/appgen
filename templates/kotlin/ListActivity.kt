package com.appgen.app.lists

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import org.jetbrains.anko.startActivity
import com.appgen.app.BR
import com.appgen.app.R
import com.appgen.app.common.Adapter
import com.appgen.app.common.ListActivity
import com.appgen.app.databinding.ActivityListBinding
import com.appgen.app.details.__Model__Activity
import com.appgen.app.models.__Model__

class __Models__Activity : ListActivity() {

    lateinit var B: ActivityListBinding

    var items = ArrayList<__Model__>()
    
    override val TAG = "__Models__Activity"
    override var paginate = false
    override var plural = "__models__"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_list)
        backToolbar(B.include?.toolbar, R.string.__models__)
        prepareList(B.recyclerView, B.swipeRefresh,
                Adapter(R.layout.item___model__, BR.__model__, items, itemClick)
        )
    }

    override fun load() {
        showWait()
        query(api().__models__()) {
            updateList(it, items)
        }
    }

    override fun detail(item: Int) {
        startActivity<__Model__Activity>(Gson().toJson(items[item]) to item_json)
    }
}
