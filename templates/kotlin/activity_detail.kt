package __PACKAGE__;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import __PACKAGE__.adapters.__CLASS_NAME__Adapter;
import __PACKAGE__.databinding.Activity__CLASS_NAME__DetailBinding;
/*__%RELATED%__import __PACKAGE__.objects.__RELATED_CLASS_NAME__;*/
import __PACKAGE__.objects.__CLASS_NAME__;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class __CLASS_NAME__DetailActivity: AppCompatActivity() {

    private var B: Activity__CLASS_NAME__DetailBinding? = null

    internal var json: String? = null
    internal var __SLUG_SINGULAR__: __CLASS_NAME__? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.__LAYOUT_NAME__)

        setSupportActionBar(B!!.include!!.toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle("__CLASS_NAME__");

        start();
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private void start() {
        json = getData()
        if (json == null) return

        val gson = Gson()
        val __SLUG_SINGULAR__ = gson.fromJson<__CLASS_NAME__>(json, __CLASS_NAME__::class.java)
        if (__SLUG_SINGULAR__ == null) return
        
        B!!.setVariable(BR.__SLUG_SINGULAR__, __SLUG_SINGULAR__)
    }

    private fun getData(): String {
        //TODO: get data
        return null
    }
}
