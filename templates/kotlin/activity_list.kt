package __PACKAGE__;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import __PACKAGE__.adapters.__CLASS_NAME__Adapter;
import __PACKAGE__.databinding.Activity__CLASS_NAME__ListBinding;
import __PACKAGE__.objects.__CLASS_NAME__;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by __AUTHOR__ on __NOW__ with APPGEN
 * __COPYRIGHT__
 */

public class __CLASS_NAME__ListActivity: AppCompatActivity() {
    
    internal lateinit var B: Activity__CLASS_NAME__ListBinding
    
    internal var json: String? = null
    internal var __CLASS_SLUG__: List<__CLASS_NAME__>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Iconify.with(new FontAwesomeModule())
        B = DataBindingUtil.setContentView(this, R.layout.__LAYOUT_NAME__)

        ssetSupportActionBar(B.toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle("__CLASS_NAME__")
        
        start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
    
    private fun start() {
        json = getData()
        if (json == null) return
        
        val gson = Gson()
        val listType = object: TypeToken<ArrayList<__CLASS_NAME__>>(){}.type
        __CLASS_SLUG__ = gson.fromJson<List<__CLASS_NAME__>>(json, listType)
        if (__CLASS_SLUG__ == null) return

        B.recyclerView.adapter = __CLASS_NAME__Adapter(
            __CLASS_SLUG__, 
            __CLASS_NAME__Adapter.OnAdapterClickListener { 
                view, position ->
                val intent = Intent(
                        this@__CLASS_NAME__ListActivity,
                        __CLASS_NAME__DetailActivity::class.java
                )
                val jsonItem = gson.toJson(__CLASS_SLUG__!![position])
                intent.putExtra("item", jsonItem)
                startActivity(intent)
            }
        )
    }

    private fun getData(): String {
        //TODO: get data
        return null
    }

    
}
