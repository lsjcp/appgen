package com.appgen.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.appgen.app.R
import com.appgen.app.common.DetailActivity
import com.appgen.app.databinding.Activity__Model__Binding
import com.appgen.app.models.__Model__
import com.google.gson.Gson

class __Model__Activity : DetailActivity() {

    lateinit var B: Activity__Model__Binding

    override val TAG = "__Model__Activity"
    override var model = "__model__"

    val className = __Model__::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity___model__)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.__model__)
        load()
    }

    override fun load() {
        print("loading __model__")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.__model__ = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().__model__(id)) {
                B.__model__ = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.__model__?.let { __model__ ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

