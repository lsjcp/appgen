package __PACKAGE__.adapters

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import __PACKAGE__.BR;
import __PACKAGE__.R;
import __PACKAGE__.objects.__CLASS_NAME__;

import java.util.List;

/**
 * Created by __AUTHOR__ on __NOW__ with APPGEN
 * __COPYRIGHT__
 */

class __CLASS_NAME__Adapter
    (private val __CLASS_SLUG__: List<__CLASS_NAME__>, private val listener: OnAdapterClickListener?): RecyclerView.Adapter<__CLASS_NAME__Adapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): __CLASS_NAME__Adapter.ViewHolder {
        val viewDataBinding = DataBindingUtil.inflate<ViewDataBinding>(
                LayoutInflater.from(parent.context), R.layout.item___CLASS_SLUG_UNIT__, parent, false)
        return ViewHolder(viewDataBinding)
    }

    override fun onBindViewHolder(holder: __CLASS_NAME__Adapter.ViewHolder, position: Int) {
        val viewDataBinding = holder.viewDataBinding
        viewDataBinding.setVariable(BR.__CLASS_SLUG_UNIT__, __CLASS_SLUG__[position])
        if (listener == null) return
        holder.viewDataBinding.root.setOnClickListener { view -> listener.onClick(view, position) }
    }

    override fun getItemCount(): Int = __CLASS_SLUG__.size

    inner class ViewHolder(val viewDataBinding: ViewDataBinding): RecyclerView.ViewHolder(viewDataBinding.root) {
        init {
            this.viewDataBinding.executePendingBindings()
        }
    }

    interface OnAdapterClickListener {
        fun onClick(view: View, position: Int)
    }
}