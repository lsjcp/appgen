package com.appgen.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.appgen.app.R
import com.appgen.app.common.EditActivity
import com.appgen.app.databinding.Activity__Model__EditBinding
import com.appgen.app.models.__Model__
import com.google.gson.Gson

class __Model__EditActivity : EditActivity() {

    lateinit var B: Activity__Model__EditBinding

    override val TAG = "__Model__EditActivity"
    override var model = "__model__"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = __Model__::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity___model___edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.__model__)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.__model__ = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().__model__(id)) {
                B.__model__ = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.__model__?.let { __model__ ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating __model__")
        var valid = true

        //Field Validation
        //__validate_fields__

        return valid;
    }

    override fun edit() {
        if (B.__model__ == null) return
        print("editing __model__")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        /* INTRON edit```
        @`String`:
        B.__model__?.__fieldCamel__ = B.input__Field__.text.toString()
        @`Int`:
        B.__model__?.__fieldCamel__ = B.input__Field__.text.toString().toIntOrNull()
        @`Double`:
        B.__model__?.__fieldCamel__ = B.input__Field__.text.toString().toDoubleOrNull()
        @`Float`:
        B.__model__?.__fieldCamel__ = B.input__Field__.text.toString().toFloatOrNull()
        ``` */

        //Edit Field
        //__edit_fields__

        val __model__ = B.__model__ ?: return
        val id = __model__.id ?: return
        mutate(api().edit__Model__(id, __model__)) { 
            B.__model__ = it
            loaded()
        }
    }

    override fun create() {
        print("creating __model__")
        showWait()
        
        B.__model__ = __Model__()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
        //__edit_fields__

        val __model__ = B.__model__ ?: return
        mutate(api().create__Model__(__model__)) { 
            B.__model__ = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.__model__?.id ?: return
        showWait()
        print("deleting __model__")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().delete__Model__(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

