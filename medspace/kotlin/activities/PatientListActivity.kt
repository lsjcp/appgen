package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.medspace.app.adapters.PatientAdapter;
import com.medspace.app.databinding.ActivityPatientListBinding;
import com.medspace.app.objects.Patient;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis J Camargo on 2018-02-23 00:04:48 with APPGEN
 * Copyright © 2018 Medspace. All rights reserved.
 */

public class PatientListActivity: AppCompatActivity() {
    
    internal lateinit var B: ActivityPatientListBinding
    
    internal var json: String? = null
    internal var patients: List<Patient>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Iconify.with(new FontAwesomeModule())
        B = DataBindingUtil.setContentView(this, R.layout.activity_patient_list)

        ssetSupportActionBar(B.toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle("Patient")
        
        start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
    
    private fun start() {
        json = getData()
        if (json == null) return
        
        val gson = Gson()
        val listType = object: TypeToken<ArrayList<Patient>>(){}.type
        patients = gson.fromJson<List<Patient>>(json, listType)
        if (patients == null) return

        B.recyclerView.adapter = PatientAdapter(
            patients, 
            PatientAdapter.OnAdapterClickListener { 
                view, position ->
                val intent = Intent(
                        this@PatientListActivity,
                        PatientDetailActivity::class.java
                )
                val jsonItem = gson.toJson(patients!![position])
                intent.putExtra("item", jsonItem)
                startActivity(intent)
            }
        )
    }

    private fun getData(): String {
        //TODO: get data
        return null
    }

    
}
