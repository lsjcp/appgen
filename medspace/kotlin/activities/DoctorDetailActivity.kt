package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.medspace.app.adapters.DoctorAdapter;
import com.medspace.app.databinding.ActivityDoctorDetailBinding;

import com.medspace.app.objects.Doctor;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class DoctorDetailActivity: AppCompatActivity() {

    private var B: ActivityDoctorDetailBinding? = null

    internal var json: String? = null
    internal var doctor: Doctor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_doctor_detail)

        setSupportActionBar(B!!.include!!.toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle("Doctor");

        start();
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private void start() {
        json = getData()
        if (json == null) return

        val gson = Gson()
        val doctor = gson.fromJson<Doctor>(json, Doctor::class.java)
        if (doctor == null) return
        
        B!!.setVariable(BR.doctor, doctor)
    }

    private fun getData(): String {
        //TODO: get data
        return null
    }
}
