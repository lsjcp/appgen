package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.medspace.app.adapters.SpecializationAdapter;
import com.medspace.app.databinding.ActivitySpecializationDetailBinding;

import com.medspace.app.objects.Specialization;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class SpecializationDetailActivity: AppCompatActivity() {

    private var B: ActivitySpecializationDetailBinding? = null

    internal var json: String? = null
    internal var specialization: Specialization? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_specialization_detail)

        setSupportActionBar(B!!.include!!.toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle("Specialization");

        start();
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private void start() {
        json = getData()
        if (json == null) return

        val gson = Gson()
        val specialization = gson.fromJson<Specialization>(json, Specialization::class.java)
        if (specialization == null) return
        
        B!!.setVariable(BR.specialization, specialization)
    }

    private fun getData(): String {
        //TODO: get data
        return null
    }
}
