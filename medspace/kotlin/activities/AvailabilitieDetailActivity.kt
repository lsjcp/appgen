package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.medspace.app.adapters.AvailabilitieAdapter;
import com.medspace.app.databinding.ActivityAvailabilitieDetailBinding;

import com.medspace.app.objects.Availabilitie;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class AvailabilitieDetailActivity: AppCompatActivity() {

    private var B: ActivityAvailabilitieDetailBinding? = null

    internal var json: String? = null
    internal var availabilitie: Availabilitie? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_availabilitie_detail)

        setSupportActionBar(B!!.include!!.toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle("Availabilitie");

        start();
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private void start() {
        json = getData()
        if (json == null) return

        val gson = Gson()
        val availabilitie = gson.fromJson<Availabilitie>(json, Availabilitie::class.java)
        if (availabilitie == null) return
        
        B!!.setVariable(BR.availabilitie, availabilitie)
    }

    private fun getData(): String {
        //TODO: get data
        return null
    }
}
