package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.medspace.app.adapters.AppointmentAdapter;
import com.medspace.app.databinding.ActivityAppointmentDetailBinding;

import com.medspace.app.objects.Appointment;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class AppointmentDetailActivity: AppCompatActivity() {

    private var B: ActivityAppointmentDetailBinding? = null

    internal var json: String? = null
    internal var appointment: Appointment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_appointment_detail)

        setSupportActionBar(B!!.include!!.toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle("Appointment");

        start();
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private void start() {
        json = getData()
        if (json == null) return

        val gson = Gson()
        val appointment = gson.fromJson<Appointment>(json, Appointment::class.java)
        if (appointment == null) return
        
        B!!.setVariable(BR.appointment, appointment)
    }

    private fun getData(): String {
        //TODO: get data
        return null
    }
}
