package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.medspace.app.adapters.Specialization_typeAdapter;
import com.medspace.app.databinding.ActivitySpecialization_typeListBinding;
import com.medspace.app.objects.Specialization_type;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis J Camargo on 2018-02-23 00:04:48 with APPGEN
 * Copyright © 2018 Medspace. All rights reserved.
 */

public class Specialization_typeListActivity: AppCompatActivity() {
    
    internal lateinit var B: ActivitySpecialization_typeListBinding
    
    internal var json: String? = null
    internal var specialization_types: List<Specialization_type>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Iconify.with(new FontAwesomeModule())
        B = DataBindingUtil.setContentView(this, R.layout.activity_specialization_type_list)

        ssetSupportActionBar(B.toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle("Specialization_type")
        
        start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
    
    private fun start() {
        json = getData()
        if (json == null) return
        
        val gson = Gson()
        val listType = object: TypeToken<ArrayList<Specialization_type>>(){}.type
        specialization_types = gson.fromJson<List<Specialization_type>>(json, listType)
        if (specialization_types == null) return

        B.recyclerView.adapter = Specialization_typeAdapter(
            specialization_types, 
            Specialization_typeAdapter.OnAdapterClickListener { 
                view, position ->
                val intent = Intent(
                        this@Specialization_typeListActivity,
                        Specialization_typeDetailActivity::class.java
                )
                val jsonItem = gson.toJson(specialization_types!![position])
                intent.putExtra("item", jsonItem)
                startActivity(intent)
            }
        )
    }

    private fun getData(): String {
        //TODO: get data
        return null
    }

    
}
