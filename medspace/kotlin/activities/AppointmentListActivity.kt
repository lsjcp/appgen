package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.medspace.app.adapters.AppointmentAdapter;
import com.medspace.app.databinding.ActivityAppointmentListBinding;
import com.medspace.app.objects.Appointment;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis J Camargo on 2018-02-23 00:04:48 with APPGEN
 * Copyright © 2018 Medspace. All rights reserved.
 */

public class AppointmentListActivity: AppCompatActivity() {
    
    internal lateinit var B: ActivityAppointmentListBinding
    
    internal var json: String? = null
    internal var appointments: List<Appointment>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Iconify.with(new FontAwesomeModule())
        B = DataBindingUtil.setContentView(this, R.layout.activity_appointment_list)

        ssetSupportActionBar(B.toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle("Appointment")
        
        start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
    
    private fun start() {
        json = getData()
        if (json == null) return
        
        val gson = Gson()
        val listType = object: TypeToken<ArrayList<Appointment>>(){}.type
        appointments = gson.fromJson<List<Appointment>>(json, listType)
        if (appointments == null) return

        B.recyclerView.adapter = AppointmentAdapter(
            appointments, 
            AppointmentAdapter.OnAdapterClickListener { 
                view, position ->
                val intent = Intent(
                        this@AppointmentListActivity,
                        AppointmentDetailActivity::class.java
                )
                val jsonItem = gson.toJson(appointments!![position])
                intent.putExtra("item", jsonItem)
                startActivity(intent)
            }
        )
    }

    private fun getData(): String {
        //TODO: get data
        return null
    }

    
}
