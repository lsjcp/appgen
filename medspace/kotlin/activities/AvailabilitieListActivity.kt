package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.medspace.app.adapters.AvailabilitieAdapter;
import com.medspace.app.databinding.ActivityAvailabilitieListBinding;
import com.medspace.app.objects.Availabilitie;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis J Camargo on 2018-02-23 00:04:48 with APPGEN
 * Copyright © 2018 Medspace. All rights reserved.
 */

public class AvailabilitieListActivity: AppCompatActivity() {
    
    internal lateinit var B: ActivityAvailabilitieListBinding
    
    internal var json: String? = null
    internal var availabilities: List<Availabilitie>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Iconify.with(new FontAwesomeModule())
        B = DataBindingUtil.setContentView(this, R.layout.activity_availabilitie_list)

        ssetSupportActionBar(B.toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle("Availabilitie")
        
        start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
    
    private fun start() {
        json = getData()
        if (json == null) return
        
        val gson = Gson()
        val listType = object: TypeToken<ArrayList<Availabilitie>>(){}.type
        availabilities = gson.fromJson<List<Availabilitie>>(json, listType)
        if (availabilities == null) return

        B.recyclerView.adapter = AvailabilitieAdapter(
            availabilities, 
            AvailabilitieAdapter.OnAdapterClickListener { 
                view, position ->
                val intent = Intent(
                        this@AvailabilitieListActivity,
                        AvailabilitieDetailActivity::class.java
                )
                val jsonItem = gson.toJson(availabilities!![position])
                intent.putExtra("item", jsonItem)
                startActivity(intent)
            }
        )
    }

    private fun getData(): String {
        //TODO: get data
        return null
    }

    
}
