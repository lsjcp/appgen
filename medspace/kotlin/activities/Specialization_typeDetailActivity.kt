package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.medspace.app.adapters.Specialization_typeAdapter;
import com.medspace.app.databinding.ActivitySpecialization_typeDetailBinding;

import com.medspace.app.objects.Specialization_type;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Specialization_typeDetailActivity: AppCompatActivity() {

    private var B: ActivitySpecialization_typeDetailBinding? = null

    internal var json: String? = null
    internal var specialization_type: Specialization_type? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_specialization_type_detail)

        setSupportActionBar(B!!.include!!.toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle("Specialization_type");

        start();
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private void start() {
        json = getData()
        if (json == null) return

        val gson = Gson()
        val specialization_type = gson.fromJson<Specialization_type>(json, Specialization_type::class.java)
        if (specialization_type == null) return
        
        B!!.setVariable(BR.specialization_type, specialization_type)
    }

    private fun getData(): String {
        //TODO: get data
        return null
    }
}
