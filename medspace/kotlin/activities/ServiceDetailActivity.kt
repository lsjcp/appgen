package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.medspace.app.adapters.ServiceAdapter;
import com.medspace.app.databinding.ActivityServiceDetailBinding;

import com.medspace.app.objects.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class ServiceDetailActivity: AppCompatActivity() {

    private var B: ActivityServiceDetailBinding? = null

    internal var json: String? = null
    internal var service: Service? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_service_detail)

        setSupportActionBar(B!!.include!!.toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle("Service");

        start();
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private void start() {
        json = getData()
        if (json == null) return

        val gson = Gson()
        val service = gson.fromJson<Service>(json, Service::class.java)
        if (service == null) return
        
        B!!.setVariable(BR.service, service)
    }

    private fun getData(): String {
        //TODO: get data
        return null
    }
}
