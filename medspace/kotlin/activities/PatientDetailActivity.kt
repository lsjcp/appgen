package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.medspace.app.adapters.PatientAdapter;
import com.medspace.app.databinding.ActivityPatientDetailBinding;

import com.medspace.app.objects.Patient;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class PatientDetailActivity: AppCompatActivity() {

    private var B: ActivityPatientDetailBinding? = null

    internal var json: String? = null
    internal var patient: Patient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_patient_detail)

        setSupportActionBar(B!!.include!!.toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle("Patient");

        start();
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private void start() {
        json = getData()
        if (json == null) return

        val gson = Gson()
        val patient = gson.fromJson<Patient>(json, Patient::class.java)
        if (patient == null) return
        
        B!!.setVariable(BR.patient, patient)
    }

    private fun getData(): String {
        //TODO: get data
        return null
    }
}
