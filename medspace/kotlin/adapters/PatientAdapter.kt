package com.medspace.app.adapters

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medspace.app.BR;
import com.medspace.app.R;
import com.medspace.app.objects.Patient;

import java.util.List;

/**
 * Created by Luis J Camargo on 2018-02-23 00:04:48 with APPGEN
 * Copyright © 2018 Medspace. All rights reserved.
 */

class PatientAdapter
    (private val patients: List<Patient>, private val listener: OnAdapterClickListener?): RecyclerView.Adapter<PatientAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PatientAdapter.ViewHolder {
        val viewDataBinding = DataBindingUtil.inflate<ViewDataBinding>(
                LayoutInflater.from(parent.context), R.layout.item_patient, parent, false)
        return ViewHolder(viewDataBinding)
    }

    override fun onBindViewHolder(holder: PatientAdapter.ViewHolder, position: Int) {
        val viewDataBinding = holder.viewDataBinding
        viewDataBinding.setVariable(BR.patient, patients[position])
        if (listener == null) return
        holder.viewDataBinding.root.setOnClickListener { view -> listener.onClick(view, position) }
    }

    override fun getItemCount(): Int = patients.size

    inner class ViewHolder(val viewDataBinding: ViewDataBinding): RecyclerView.ViewHolder(viewDataBinding.root) {
        init {
            this.viewDataBinding.executePendingBindings()
        }
    }

    interface OnAdapterClickListener {
        fun onClick(view: View, position: Int)
    }
}