package com.medspace.app.objects;


/**
 * Created by Luis J Camargo on 2018-02-21 02:37:24
 * Copyright © 2018 Medspace. All rights reserved.
 */

public class Meditation {

    public int id;
    public Object[] category;
    public Object speaker;
    public String title;
    public String slug;
    public String description;
    public boolean free;
    public int order;
    public String date;
    public String dateRecorded;
    public String createdAt;
    public String updatedAt;
    public String audio;
    public String image;
    public String status;
    public int duration;


    
}
