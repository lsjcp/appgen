package com.medspace.app.objects;


/**
 * Created by Luis J Camargo on 2018-02-21 02:37:23
 * Copyright © 2018 Medspace. All rights reserved.
 */

public class Event {

    public String url;
    public int id;
    public String title;
    public String description;
    public String place;
    public String dateStart;
    public String dateEnd;
    public String image;


    
}
