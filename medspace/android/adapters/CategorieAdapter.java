package com.medspace.app.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medspace.app.BR;
import com.medspace.app.R;
import com.medspace.app.objects.Categorie;

import java.util.List;

/**
 * Created by Luis J Camargo on 2018-02-21 02:37:23 with APPGEN
 */

public class CategorieAdapter extends RecyclerView.Adapter<CategorieAdapter.ViewHolder> {

    private final List<Categorie> categories;
    private final OnAdapterClickListener listener;

    public CategorieAdapter(List<Categorie> categories, OnAdapterClickListener listener) {
        this.categories = categories;
        this.listener = listener;
    }

    @Override
    public CategorieAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.item_categorie, parent, false);
        return new ViewHolder(viewDataBinding);
    }

    @Override
    public void onBindViewHolder(CategorieAdapter.ViewHolder holder, final int position) {
        ViewDataBinding viewDataBinding = holder.getViewDataBinding();
        viewDataBinding.setVariable(BR.categorie, categories.get(position));
        if (listener == null) return;
        holder.view.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(view, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final ViewDataBinding view;

        public ViewHolder(ViewDataBinding view) {
            super(view.getRoot());
            this.view = view;
            this.view.executePendingBindings();
        }

        public ViewDataBinding getViewDataBinding() {
            return view;
        }
    }

    public interface OnAdapterClickListener {
        void onClick(View view, int position);
    }
}