package com.medspace.app.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medspace.app.BR;
import com.medspace.app.R;
import com.medspace.app.objects.Speaker;

import java.util.List;

/**
 * Created by Luis J Camargo on 2018-02-21 02:37:23 with APPGEN
 */

public class SpeakerAdapter extends RecyclerView.Adapter<SpeakerAdapter.ViewHolder> {

    private final List<Speaker> speakers;
    private final OnAdapterClickListener listener;

    public SpeakerAdapter(List<Speaker> speakers, OnAdapterClickListener listener) {
        this.speakers = speakers;
        this.listener = listener;
    }

    @Override
    public SpeakerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.item_speaker, parent, false);
        return new ViewHolder(viewDataBinding);
    }

    @Override
    public void onBindViewHolder(SpeakerAdapter.ViewHolder holder, final int position) {
        ViewDataBinding viewDataBinding = holder.getViewDataBinding();
        viewDataBinding.setVariable(BR.speaker, speakers.get(position));
        if (listener == null) return;
        holder.view.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(view, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return speakers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final ViewDataBinding view;

        public ViewHolder(ViewDataBinding view) {
            super(view.getRoot());
            this.view = view;
            this.view.executePendingBindings();
        }

        public ViewDataBinding getViewDataBinding() {
            return view;
        }
    }

    public interface OnAdapterClickListener {
        void onClick(View view, int position);
    }
}