package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.medspace.app.adapters.MeditationAdapter;
import com.medspace.app.databinding.ActivityMeditationDetailBinding;

import com.medspace.app.objects.Meditation;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MeditationDetailActivity extends AppCompatActivity {

    ActivityMeditationDetailBinding B;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        B = DataBindingUtil.setContentView(this, R.layout.activity_meditation_detail);

        setSupportActionBar(B.include.toolbar);
        getSupportActionBar().setIcon(R.drawable.icon);
        setTitle("Meditation");

        start();
    }

    private void start() {
        String json = "";
        try {
            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<Meditation>>(){}.getType();
            List<Meditation> meditation = gson.fromJson(json, listType);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
