package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.medspace.app.adapters.SpecializationAdapter;
import com.medspace.app.databinding.ActivitySpecializationListBinding;
import com.medspace.app.objects.Specialization;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SpecializationListActivity extends AppCompatActivity {
    
    ActivitySpecializationListBinding B;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Iconify.with(new FontAwesomeModule());
        B = DataBindingUtil.setContentView(this, R.layout.activity_specialization_list);

        setSupportActionBar(B.include.toolbar);
        getSupportActionBar().setIcon(R.drawable.icon);
        setTitle("Specializations");
        
        start();
        
    }
    
    private void start() {
        String json = "";
        if (json != null) {
            try {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<Specialization>>(){}.getType();
                List<Specialization> specialization = gson.fromJson(json, listType);
                B.recyclerView.setAdapter(
                        new SpecializationAdapter(specialization, new SpecializationAdapter.OnAdapterClickListener() {
                            @Override
                            public void onClick(View view, int position) {

                            }
                        })
                );
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    
}
