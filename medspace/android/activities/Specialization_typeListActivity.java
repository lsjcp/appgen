package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.medspace.app.adapters.Specialization_typeAdapter;
import com.medspace.app.databinding.ActivitySpecialization_typeListBinding;
import com.medspace.app.objects.Specialization_type;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Specialization_typeListActivity extends AppCompatActivity {
    
    ActivitySpecialization_typeListBinding B;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Iconify.with(new FontAwesomeModule());
        B = DataBindingUtil.setContentView(this, R.layout.activity_specialization_type_list);

        setSupportActionBar(B.include.toolbar);
        getSupportActionBar().setIcon(R.drawable.icon);
        setTitle("Specialization_types");
        
        start();
        
    }
    
    private void start() {
        String json = "";
        if (json != null) {
            try {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<Specialization_type>>(){}.getType();
                List<Specialization_type> specialization_type = gson.fromJson(json, listType);
                B.recyclerView.setAdapter(
                        new Specialization_typeAdapter(specialization_type, new Specialization_typeAdapter.OnAdapterClickListener() {
                            @Override
                            public void onClick(View view, int position) {

                            }
                        })
                );
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    
}
