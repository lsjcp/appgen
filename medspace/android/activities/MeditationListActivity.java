package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.medspace.app.adapters.MeditationAdapter;
import com.medspace.app.databinding.ActivityMeditationListBinding;
import com.medspace.app.objects.Meditation;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MeditationListActivity extends AppCompatActivity {
    
    ActivityMeditationListBinding B;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Iconify.with(new FontAwesomeModule());
        B = DataBindingUtil.setContentView(this, R.layout.activity_meditation_list);

        setSupportActionBar(B.include.toolbar);
        getSupportActionBar().setIcon(R.drawable.icon);
        setTitle("Meditations");
        
        start();
        
    }
    
    private void start() {
        String json = "";
        if (json != null) {
            try {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<Meditation>>(){}.getType();
                List<Meditation> meditation = gson.fromJson(json, listType);
                B.recyclerView.setAdapter(
                        new MeditationAdapter(meditation, new MeditationAdapter.OnAdapterClickListener() {
                            @Override
                            public void onClick(View view, int position) {

                            }
                        })
                );
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    
}
