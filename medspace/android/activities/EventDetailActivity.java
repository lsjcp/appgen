package com.medspace.app;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.medspace.app.adapters.EventAdapter;
import com.medspace.app.databinding.ActivityEventDetailBinding;

import com.medspace.app.objects.Event;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EventDetailActivity extends AppCompatActivity {

    ActivityEventDetailBinding B;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        B = DataBindingUtil.setContentView(this, R.layout.activity_event_detail);

        setSupportActionBar(B.include.toolbar);
        getSupportActionBar().setIcon(R.drawable.icon);
        setTitle("Event");

        start();
    }

    private void start() {
        String json = "";
        try {
            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<Event>>(){}.getType();
            List<Event> event = gson.fromJson(json, listType);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
