const http = require('http');
const request = require('request');
var fs = require('fs');
const mkdirp = require('mkdirp');
const camelCase = require('camelcase');
const decamelize = require('decamelize');
var cache = {};

var platform_data = {
	'kotlin': {
		'class_ext':'kt',
		'extension':'kt',
		'layout_extension':'xml',
		'controller':'Activity',
		'types': {
			'nested object':'__className__',
			'datetime':'DateTime',
			'field':'String',
			'string':'String',
			'integer':'Int',
			'boolean':'Boolean',
			'__def__':'String'
		},
		'files': {
			'model':'Model.kt',
			'list':'ListActivity.kt',
			'detail':'DetailActivity.kt',
			'edit':'EditActivity.kt',
			'layout_list':'item_list.xml',
			'layout_detail':'activity_detail.xml',
			'layout_edit':'activity_edit.xml',
			'android_manifest':'AndroidManifest.xml',
			'android_strings':'strings.xml',
			'api_service':'ApiService.kt',
			'strings':'strings.xml',
			'menu':'drawer.xml'
		},
		'paths': {

		},
		'templates': {
			'field_valid':
		`
		//__field__ validation
        val __fieldCamel__ = B.input__Field__.text.toString()
        if (__fieldCamel__.isNullOrBlank()) {
            valid = false
            throwField(B.input__Field__, "EMPTY")
        }
		`,
			'field_edit_String':
		`
		B.__model__?.__fieldCamel__ = B.input__Field__.text.toString()
		`,
			'field_edit_Int':
		`
		B.__model__?.__fieldCamel__ = B.input__Field__.text.toString().toIntOrNull()
		`,
			'field_edit_Double':
		`
		B.__model__?.__fieldCamel__ = B.input__Field__.text.toString().toDoubleOrNull()
		`,
		'field_edit_Float':
		`
		B.__model__?.__fieldCamel__ = B.input__Field__.text.toString().toFloatOrNull()
		`,

		//DESCRIPTION FIELDS
		//String
			'layout_field_String':
			`
		<TextView
			android:layout_width="match_parent"
			android:layout_height="wrap_content"
			style="@style/text"
			tools:text="@string/placeholder"
			app:hideNull="@{__model__.__fieldCamel__}"
			android:text="@{__model__.__fieldCamel__}"/>
		`,
		//DateTime
			'layout_field_DateTime':
			`
		<TextView
			android:layout_width="match_parent"
			android:layout_height="wrap_content"
			style="@style/text"
			tools:text="@string/placeholder"
			app:dateTime="@{__model__.__fieldCamel__}"/>
		`,
		//Int
			'layout_field_Int':
		`
		<TextView
			android:layout_width="match_parent"
			android:layout_height="wrap_content"
			style="@style/text"
			tools:text="@string/placeholder"
			app:hideNull="@{__model__.__fieldCamel__}"
			android:text="@{\`\` + __model__.__fieldCamel__}"/>
		`,
		//Double
			'layout_field_Double':
		`
		<TextView
			android:layout_width="match_parent"
			android:layout_height="wrap_content"
			style="@style/text"
			tools:text="@string/placeholder"
			app:hideNull="@{__model__.__fieldCamel__}"
			android:text="@{\`\` + __model__.__fieldCamel__}"/>
		`,
		//Float
			'layout_field_Float':
		`
		<TextView
			android:layout_width="match_parent"
			android:layout_height="wrap_content"
			style="@style/text"
			tools:text="@string/placeholder"
			app:hideNull="@{__model__.__fieldCamel__}"
			android:text="@{\`\` + __model__.__fieldCamel__}"/>
		`,

		//EDITOR FIELDS
		//String
		'layout_edit_field_String':
			`
		<com.google.android.material.textfield.TextInputLayout
			android:layout_width="match_parent"
			android:layout_height="wrap_content"
			android:layout_marginTop="@dimen/space">

			<com.google.android.material.textfield.TextInputEditText
				android:id="@+id/input___field__"
				android:layout_width="match_parent"
				android:layout_height="wrap_content"
				android:inputType="text"
				android:text="@{__model__.__fieldCamel__}"
				android:hint="@string/hint_string"/>

		</com.google.android.material.textfield.TextInputLayout>
		`,
		//DateTime
			'layout_edit_field_DateTime':
			`
		<com.google.android.material.textfield.TextInputLayout
			android:layout_width="match_parent"
			android:layout_height="wrap_content"
			android:layout_marginTop="@dimen/space">

			<com.google.android.material.textfield.TextInputEditText
				android:id="@+id/input___field__"
				android:layout_width="match_parent"
				android:layout_height="wrap_content"
				android:inputType="text"
				android:clickable="false" 
        		android:cursorVisible="false" 
        		android:focusable="false" 
        		android:focusableInTouchMode="false"
				app:dateTime="@{__model__.__fieldCamel__}"
				android:hint="@string/hint_datetime"/>

		</com.google.android.material.textfield.TextInputLayout>
		`,
		//Int
			'layout_edit_field_Int':
		`
		<com.google.android.material.textfield.TextInputLayout
			android:layout_width="match_parent"
			android:layout_height="wrap_content"
			android:layout_marginTop="@dimen/space">

			<com.google.android.material.textfield.TextInputEditText
				android:id="@+id/input___field__"
				android:layout_width="match_parent"
				android:layout_height="wrap_content"
				android:inputType="number"
				android:text="@{\`\` + __model__.__fieldCamel__}"
				android:hint="@string/hint_int"/>

		</com.google.android.material.textfield.TextInputLayout>
		`,
		//Double
			'layout_edit_field_Double':
		`
		<com.google.android.material.textfield.TextInputLayout
			android:layout_width="match_parent"
			android:layout_height="wrap_content"
			android:layout_marginTop="@dimen/space">

			<com.google.android.material.textfield.TextInputEditText
				android:id="@+id/input___field__"
				android:layout_width="match_parent"
				android:layout_height="wrap_content"
				android:inputType="numberDecimal"
				android:text="@{\`\` + __model__.__fieldCamel__}"
				android:hint="@string/hint_double"/>

		</com.google.android.material.textfield.TextInputLayout>
		`,
		//Float
			'layout_edit_field_Float':
		`
		<com.google.android.material.textfield.TextInputLayout
			android:layout_width="match_parent"
			android:layout_height="wrap_content"
			android:layout_marginTop="@dimen/space">

			<com.google.android.material.textfield.TextInputEditText
				android:id="@+id/input___field__"
				android:layout_width="match_parent"
				android:layout_height="wrap_content"
				android:inputType="numberSigned"
				android:text="@{\`\` + __model__.__fieldCamel__}"
				android:hint="@string/hint_float"/>

		</com.google.android.material.textfield.TextInputLayout>
		`
		}
	},
	'swift': {
		'class_ext':'swift',
		'types': {
			'nested object':'__className__',
			'String':'String',
			'Integer':'Int',
			'Double':'Double',
			'Boolean':'Bool',
			'Array':'[Any]',
			'__def__':'Any'
		},
		'templates': {
			'field':'@decorator declare name: Type init //comment'
		}
	}
}


var file = process.argv[2];
var path = require('path');
var fs = require("fs");
var json = fs.readFileSync(path.resolve(__dirname,file));
var config = JSON.parse(json);
cache.classes = [];

function start() {
	const lang = 'kotlin';
	config.endpoints.forEach(item => {
		doEndpoint(lang, config.apiRestRoot, item)
	});
	config.abstracts.forEach(item =>{
		console.log(`abstract ${item.name}`);
		createModel(lang, item.name, item.plural, item.fields, item.parent);
	});
	createAppendix(lang);
}

function doEndpoint(lang, root, plural) {
	console.log(`doing ${plural} endpoint`);
	getApiOptions(root + plural, config.bearerToken, (error, response, body) => {
		if (error) {
			console.log(`error on request`);
			console.log(body);
		} else {
			console.log(`>>>${body.name}`);
			var fields = body.actions.POST;
			createEntry(lang, body.name, plural, fields);
		}
	})
}

function createEntry(lang, name, plural, fields) {
	console.log(`>>>creating entry ${name}`);
	name = name.replace(" List", "");
	cache.classes.push(getClassName(name));

	createModel(lang, name, plural, fields);
	createList(lang, name, plural);
	createListLayout(lang, name, plural, fields);
	createDetail(lang, name, plural, fields);
	createDetailLayout(lang, name, plural, fields);
	createEdit(lang, name, plural, fields);
	createEditLayout(lang, name, plural, fields);
}

function createAppendix(lang) {
	createManifest(lang);
	createApiService(lang);
	createStrings(lang);
	createMenus(lang);
}

function createManifest(lang) {
	if (lang == "swift") return;
	const scope = platform_data[lang];
	const file = scope.files.android_manifest;
	const template = 'templates/' + lang + '/' + file;
	
	fs.readFile(template, 'utf8', function(err, data) {  
		if (err) throw err;
		data = getCommentIntron(fillGenerals(data), (name, intron) => {
			var entries = '';
			config.models.forEach(singular => {
				const plural = getPluralSlug(singular);
				const modelSingular = getClassName(singular.replace(/ .*/,''));
				const modelPlural = getClassNamePlural(singular);
				var template2 = intron;
				template2 = fillModel(fillGenerals(template2), modelSingular, modelPlural, singular, plural);
				entries += `${template2}\n`;
			});
			return entries;
		});
		const dir = `${config.workspace}/${lang}/`;
		const path = file;
		saveFile(dir, path, data);
	});
}

function createApiService(lang) {
	if (lang == "swift") return;
	const scope = platform_data[lang];
	const file = scope.files.api_service
	const template = 'templates/' + lang + '/' + file;
	fs.readFile(template, 'utf8', function(err, data) {  
		if (err) throw err;
		data = getCommentIntron(fillGenerals(data), (name, intron) => {
			var entries = '';
			config.models.forEach(singular => {
				const plural = getPluralSlug(singular);
				const modelSingular = getClassName(singular.replace(/ .*/,''));
				const modelPlural = getClassNamePlural(singular);
				var template2 = intron;
				template2 = fillModel(fillGenerals(template2), modelSingular, modelPlural, singular, plural);
				entries += `${template2}\n`;
			});
			return entries;
		});
		const dir = `${config.workspace}/${lang}/api/`;
		const path = file;
		saveFile(dir, path, data);
	});
}

function createStrings(lang) {
	if (lang == "swift") return;
	const scope = platform_data[lang];
	const file = scope.files.strings;
	const template = 'templates/' + lang + '/' + file;
	fs.readFile(template, 'utf8', function(err, data) {  
		if (err) throw err;
		data = getCommentIntron(fillGenerals(data), (name, intron) => {
			var entries = '';
			config.models.forEach(singular => {
				const plural = getPluralSlug(singular);
				const modelSingular = getClassName(singular.replace(/ .*/,''));
				const modelPlural = getClassNamePlural(singular);
				var replaced = fillModel(fillGenerals(intron), modelSingular, modelPlural, singular, plural);
				entries += `${replaced}\n`;
			});
			return entries;
		});
		const dir = `${config.workspace}/${lang}/strings/`;
		const path = file;
		saveFile(dir, path, data);
	});
}

function createMenus(lang) {
	if (lang == "swift") return;
	const scope = platform_data[lang];
	const file = scope.files.menu;
	const template = 'templates/' + lang + '/' + file;
	fs.readFile(template, 'utf8', function(err, data) {  
		if (err) throw err;
		data = getCommentIntron(fillGenerals(data), (name, intron) => {
			console.log(`intron match named ${name} ${intron}`);
			var entries = '';
			config.models.forEach(singular => {
				var replaced = fillNamedModel(fillGenerals(intron), modelNames(singular));
				entries += `${replaced}\n`;
			});
			return entries;
		});
		const dir = `${config.workspace}/${lang}/menus/`;
		const path = file;
		saveFile(dir, path, data);
	});
}

function getCommentIntron(string, replacer) {
	//Java/Kotlin/Swift Comment Replacer
	var all = string.match(/(\/\* INTRON )([\S\s]*?)```([\S\s]*?)(``` \*\/)/g);
	if (all != undefined && all.length > 0) {
		all.forEach(ocurrence => {
			const match = ocurrence.match(/(\/\* INTRON )([\S\s]*?)```([\S\s]*?)(``` \*\/)/);
			const [whole, header, name, content, footer] = match;
			console.log(`matching ${header} ${name} ${content.length} whole? ${whole.length}`);
			string = string.replace(whole, replacer(name, content));
		});
	}
	//XML Replacer
	all = string.match(/(<!-- INTRON )([\S\s]*?)```([\S\s]*?)(``` -->)/g);
	if (all != undefined && all.length > 0) {
		all.forEach(ocurrence => {
			const match = ocurrence.match(/(<!-- INTRON )([\S\s]*?)```([\S\s]*?)(``` -->)/);
			const [whole, header, name, content, footer] = match;
			console.log(`matching ${header} ${name} ${content.length} whole? ${whole.length}`);
			string = string.replace(whole, replacer(name, content));
		});
	}
	return string;
}

function getSwitch(string, replacer) {
	var matches = string.match(/@`([\S]*?)`:\n([\S\s]*?$)/g);
	if (matches != undefined) {
		var cases = {};
		matches.forEach(ocurrence => {
			const match = ocurrence.match(/@`([\S]*?)`:\n([\S\s]*?$)/);
			const [whole, name, content] = match;
			cases[name] = content;
			console.log(`matching gene switch ${name} ${content.length} whole? ${whole.length}`);
		});
		return replacer(cases);
	}
	return "";
}

function excludeChildFields(fields, parent) {
	if (parent == null || parent == undefined) return fields;
	var response;
	config.abstracts.forEach(abstract => {
		if (abstract.name == parent && abstract.fields != null) {
			console.log("looping on abstract class ->" + abstract.name);
			for (var [key, value] of Object.entries(fields)) {
				if (key in abstract.fields) {
					console.log("deleting " + key);
					delete fields[key];
				} else {
					console.log("stays " + key);
				}
			}
			//if (fields == null || fields == undefined) return null;
			response = excludeChildFields(fields, abstract.parent);
			//response = fields;
		}
	});
	//console.log("end loop " + response);
	return response;
}

function createModel(lang, name, plural, fields, parent) {
	console.log(`>>>creating model ${name}`);
	const scope = platform_data[lang];
	const modelSingular = getClassName(name);
	const modelPlural = getClassName(plural);
	const singular = decamelize(modelSingular);
	const template = 'templates/' + lang + '/' + scope.files.model;
	console.log("modelSingular >>> " + modelSingular);
	console.log(`class >>> ${singular}: ${config.parents[singular]}`);
	parent = (parent || config.parents[singular]);
	var parentClass = getClassName(parent || '');
	if (parentClass.length > 0) parentClass = `: ${parentClass}`;
	if (fields != null && fields != undefined) {
		console.log("fields pre exlcude count " + Object.keys(fields));
		fields = excludeChildFields(fields, parent);
		console.log("fields?" + fields);
		if (fields != null) {
			console.log("fields post exlcude count " + Object.keys(fields)); 
		} else {
			console.log("fields returned were null");
		}

	}
	fs.readFile(template, 'utf8', function(err, data) {  
		if (err) throw err;
		data = fillModel(fillGenerals(data), modelSingular, modelPlural, singular, plural);
		data = multiReplace(data, [
			[": __Parent__()", parentClass],
		]);
		data = getCommentIntron(data, (name, intron) => {
			var output = ""
			if (fields == null) return output;
			for (var [key, value] of Object.entries(fields)) {
				const type = fieldClass(lang, value.class, value.required) || fieldType(lang, value.type, value.required);
				const def = fieldDefault(lang, fieldType(lang, value.type, false), value.default);
				var template2 = intron;
				template2 = multiReplace(template2, [
					["@decorator ", fieldDecorator(lang, key)],
					["declare", fieldDeclare(lang, key, true)],
					["name", camelCase(key)],
					["Type", type],
					["init", fieldInit(lang, value.required, def)],
					[" //comment", fieldComment(lang, value.label)],
					["__className__", getClassName(key)]
				]);
				output += `${template2}\n`;
			}
			return output;
		});
		const dir = `${config.workspace}/${lang}/models`;
		const path = `${modelSingular}.${scope.extension}`;
		saveFile(dir, path, data);
	});
}

function createLayoutFields(lang, model, fields, detailed) {
	var indent = "    ";
	var output = ""
	const show = config.fields.show || [];
	const hide = config.fields.hide || [];
	for (var [key, value] of Object.entries(fields)) {
		var type = typeToType(value.type, lang)
		var allowed = (detailed || show.includes(key)) && !hide.includes(key);
		if (["DateTime", "String"].includes(type) && allowed) {
			var template = createLayoutField(lang, type, model, key)
			output += `${indent}${template}\n`;
		}
	}
	return output;
}

function createLayoutField(lang, type, model, field) {
	console.log(`>>>creating field layout of ${field}`);
	const scope = platform_data[lang];
	const modelSingular = getClassName(model.replace(/ .*/,''));
	const singular = decamelize(modelSingular);
	const template = scope.templates[`layout_field_${type}`];
	return fillFields(template, modelSingular, singular, field);
}

function createLayoutEditFields(lang, model, fields, detailed) {
	var indent = "    ";
	var output = ""
	const show = config.fields.show || [];
	const hide = config.fields.hide || [];
	for (var [key, value] of Object.entries(fields)) {
		var type = typeToType(value.type, lang);
		var allowed = (detailed || show.includes(key)) && !hide.includes(key);
		if (["DateTime", "String", "Int", "Float", "Double"].includes(type) && allowed) {
			var template = createLayoutEditField(lang, type, model, key)
			output += `${indent}${template}\n`;
		}
	}
	return output;
}

function createLayoutEditField(lang, type, model, field) {
	console.log(`>>>creating edit field layout of ${field}`);
	const scope = platform_data[lang];
	const modelSingular = getClassName(model.replace(/ .*/,''));
	const singular = decamelize(modelSingular);
	const template = scope.templates[`layout_edit_field_${type}`];
	return fillFields(template, modelSingular, singular, field);
}

function createEditFieldsCode(lang, model, fields, detailed) {
	var indent = "    ";
	var output = ""
	const show = config.fields.show || [];
	const hide = config.fields.hide || [];
	for (var [key, value] of Object.entries(fields)) {
		var type = typeToType(value.type, lang)
		var allowed = (detailed || show.includes(key)) && !hide.includes(key);
		if (["DateTime", "String", "Int", "Float", "Double"].includes(type) && allowed) {
			var template = createEditFieldCode(lang, type, model, key)
			output += `${indent}${template}\n`;
		}
	}
	return output;
}

function createEditFieldCode(lang, type, model, field) {
	console.log(`>>>creating field edit code of ${field}: ${type}`);
	const scope = platform_data[lang];
	const modelSingular = getClassName(model.replace(/ .*/,''));
	const singular = decamelize(modelSingular);
	const template = scope.templates[`field_edit_` + type];
	return fillFields(template, modelSingular, singular, field);
}

function createValidFieldsCode(lang, model, fields, detailed) {
	var indent = "    ";
	var output = ""
	const show = config.fields.show || [];
	const hide = config.fields.hide || [];
	for (var [key, value] of Object.entries(fields)) {
		var type = typeToType(value.type, lang);
		var allowed = (detailed || show.includes(key)) && !hide.includes(key);
		if (["DateTime", "String", "Int", "Float", "Double"].includes(type) && allowed) {
			var template = createValidFieldCode(lang, type, model, key)
			output += `${indent}${template}\n`;
		}
	}
	return output;
}

function createValidFieldCode(lang, type, model, field) {
	console.log(`>>>creating field validate code of ${field}`);
	const scope = platform_data[lang];
	const modelSingular = getClassName(model.replace(/ .*/,''));
	const singular = decamelize(modelSingular);
	const template = scope.templates[`field_valid`];
	return fillFields(template, modelSingular, singular, field);
}

function modelNames(singular) {
	var names = {};
	names.snakeSingular = decamelize(singular);
	names.snakePlural = getPluralSlug(names.snakeSingular);
	names.classSingular = getClassName(names.snakeSingular);
	names.classPlural = getClassNamePlural(names.snakeSingular);
	names.camelSingular = camelCase(names.snakeSingular);
	names.camelPlural = camelCase(names.snakePlural);
	return names;
}

function fillFields(template, model, singular, field) {
	if (template == undefined) return ``;
	return multiReplace(template, [
		["__Model__", model],
		["__model__", singular],
		["__field__", field],
		["__fieldCamel__", camelCase(field)],
		["__Field__", getClassName(field)]
	]);
}

function fillModel(template, model, models, singular, plural) {
	if (template == undefined) return ``;
	return multiReplace(template, [
		["__Model__", model],
		["__Models__", models],
		["__model__", singular],
		["__models__", plural]
	]);
}

function fillNamedModel(template, names) {
	if (template == undefined) return ``;
	return multiReplace(template, [
		["__Model__", names.classSingular],
		["__Models__", names.classPlural],
		["__model__", names.snakeSingular],
		["__models__", names.snakePlural]
	]);
}

function fillAll(template, model, models, singular, plural) {
	return fillModel(fillGenerals(template), model, models, singular, plural);
}

function multiReplace(str, arr) {
	for (var [a, b] of arr) str = str.replaceAll(a, b)
	return str;
}

function fieldDecorator(lang, name) {
	if (lang == 'kotlin') return (name != camelCase(name)) ? `@SerializedName("${name}") ` : ``;
	if (lang == 'swift') return '';
}	

function fieldDeclare(lang, value, mutable) {
	if (lang == 'kotlin') return mutable ? 'var' : 'val';
	if (lang == 'swift') return mutable ? 'var' : 'let';
}

function fieldType(lang, type, required) {
	if (lang == 'kotlin') return typeToType(type, lang) + (required ? "" : "?");
	if (lang == 'swift') return typeToType(type, lang) + (required ? "" : "?");
}

function fieldClass(lang, type, required) {
	if (type == undefined) return null;
	if (lang == 'kotlin') return type + (required ? "" : "?");
	if (lang == 'swift') return type + (required ? "" : "?");
}

function fieldInit(lang, required, def) {
	if (lang == 'kotlin') return def ? `= ${def}` : (required ? `` : `= null`);
	if (lang == 'swift') return def ? `= ${def}` : ``;
}

function fieldDefault(lang, type, def) {
	if (lang == 'kotlin') {
		if (type == "String") return `"${def}"`;
		if (type == "Float") return `${def}f`;
		if (type == "Boolean") return (def == "True") ? "true" : "false"
		return def;
	}
	if (lang == 'swift') {
		if (type == "String") return `"${def}"`;
		if (type == "Float") return `${def}f`;
		if (type == "Boolean") return (def == "True") ? "True" : "False"
		return def;
	}
}

function fieldComment(lang, comment) {
	if (lang == 'kotlin') return comment ? ` // ${comment}` : ``;
	if (lang == 'swift') return comment ? ` // ${comment}` : ``;
}

function createList(lang, name, plural) {
	console.log(`>>>creating list of ${plural}`);
	const scope = platform_data[lang];
	const modelSingular = getClassName(name.replace(/ .*/,''));
	const modelPlural = getClassName(plural);
	const singular = decamelize(modelSingular);
	const template = 'templates/' + lang + '/' + scope.files.list;
	fs.readFile(template, 'utf8', function(err, data) { 
		if (err) throw err;
		data = fillAll(data, modelSingular, modelPlural, singular, plural);
		const dir = `${config.workspace}/${lang}/lists`;
		const path = `${modelPlural}${scope.controller}.${scope.extension}`;
		saveFile(dir, path, data);
	});
}

function fillGenerals(template) {
	return multiReplace(template, [
		["com.appgen.app", config.androidPackage],
		["__author__", config.author],
		["__copyright__", config.copyright],
		["__now__", now()]
	]);
}

function createListLayout(lang, name, plural, fields) {
	console.log(`>>>creating list layout of ${plural}`);
	const scope = platform_data[lang];
	const modelSingular = getClassName(name.replace(/ .*/,''));
	const modelPlural = getClassName(plural);
	const singular = decamelize(modelSingular);
	const fieldsLayout = createLayoutFields(lang, modelSingular, fields, false);
	const template = 'templates/' + lang + '/' + scope.files.layout_list;
	fs.readFile(template, 'utf8', function(err, data) { 
		if (err) throw err;
		data = fillAll(data, modelSingular, modelPlural, singular, plural);
		data = multiReplace(data, [
			["<!-- fields -->", fieldsLayout]
		]);

		const dir = `${config.workspace}/${lang}/layout`;
		const path = `item_${singular}.${scope.layout_extension}`;
		saveFile(dir, path, data);
	});
}

function createDetail(lang, name, plural, fields) {
	console.log(`>>>creating detail of ${plural}`);
	const scope = platform_data[lang];
	const modelSingular = getClassName(name.replace(/ .*/,''));
	const modelPlural = getClassName(plural);
	const singular = decamelize(modelSingular);
	const template = 'templates/' + lang + '/' + scope.files.detail;
	fs.readFile(template, 'utf8', function(err, data) { 
		if (err) throw err;
		data = fillAll(data, modelSingular, modelPlural, singular, plural);
		const dir = `${config.workspace}/${lang}/details`;
		const path = `${modelSingular}${scope.controller}.${scope.extension}`;
		saveFile(dir, path, data);
	});
}

function createDetailLayout(lang, name, plural, fields) {
	console.log(`>>>creating detail layout of ${name}`);
	const scope = platform_data[lang];
	const modelSingular = getClassName(name.replace(/ .*/,''));
	const modelPlural = getClassName(plural);
	const singular = decamelize(modelSingular);
	const fieldsLayout = createLayoutFields(lang, modelSingular, fields, true);
	const template = 'templates/' + lang + '/' + scope.files.layout_detail;
	fs.readFile(template, 'utf8', function(err, data) { 
		if (err) throw err;
		data = fillAll(data, modelSingular, modelPlural, singular, plural);
		data = multiReplace(data, [
			["<!-- fields -->", fieldsLayout]
		]);
		const dir = `${config.workspace}/${lang}/layout`;
		const path = `activity_${singular}.${scope.layout_extension}`;
		saveFile(dir, path, data);
	});
}

function createEdit(lang, name, plural, fields) {
	console.log(`>>>creating edit of ${plural}`);
	const scope = platform_data[lang];
	const modelSingular = getClassName(name.replace(/ .*/,''));
	const modelPlural = getClassName(plural);
	const singular = decamelize(modelSingular);
	const fieldsEdit = createEditFieldsCode(lang, modelSingular, fields, true);
	const fieldsValid = createValidFieldsCode(lang, modelSingular, fields, true);
	const template = 'templates/' + lang + '/' + scope.files.edit;
	fs.readFile(template, 'utf8', function(err, data) { 
		if (err) throw err;
		data = fillAll(data, modelSingular, modelPlural, singular, plural);
		data = multiReplace(data, [
			["//__edit_fields__", fieldsEdit],
			["//__validate_fields__", fieldsValid]
		]);
		data = getCommentIntron(data, (name, intron) => {
			if (name == 'edit') {
				var templates = getSwitch(intron, cases => {
					var output = ""
					for (var [key, value] of Object.entries(fields)) {
						var type = typeToType(value.type, lang)
						const show = config.fields.show || [];
						const hide = config.fields.hide || [];
						var allowed = (show.includes(key)) && !hide.includes(key);
						if (allowed) {
							var replaced = "";
							const intemplate = cases[type] || cases.__def__;
							if (intemplate != undefined && intemplate != null) {
								replaced = fillFields(intemplate, modelSingular, singular, field);
								output += `${indent}${replaced}\n`;
							}
						}
					}
					return output;
				});
				return templates;
			} 
		});
		const dir = `${config.workspace}/${lang}/editors`;
		const path = `${modelSingular}Edit${scope.controller}.${scope.extension}`;
		saveFile(dir, path, data);
	});
}

function createEditLayout(lang, name, plural, fields) {
	console.log(`>>>creating edit layout of ${name}`);
	const scope = platform_data[lang];
	const modelSingular = getClassName(name.replace(/ .*/,''));
	const modelPlural = getClassName(plural);
	const singular = decamelize(modelSingular);
	const fieldsLayout = createLayoutEditFields(lang, modelSingular, fields, true);
	const template = 'templates/' + lang + '/' + scope.files.layout_edit;
	fs.readFile(template, 'utf8', function(err, data) { 
		if (err) throw err;
		data = fillAll(data, modelSingular, modelPlural, singular, plural);
		data = multiReplace(data, [
			["<!-- fields -->", fieldsLayout]
		]);
		const dir = `${config.workspace}/${lang}/layout`;
		const path = `activity_${singular}_edit.${scope.layout_extension}`;
		saveFile(dir, path, data);
	});
}

function readFiles(dirname, onFileContent, onError) {
  fs.readdir(dirname, function(err, filenames) {
    if (err) {
      onError(err);
      return;
    }
    filenames.forEach(function(filename) {
      fs.readFile(dirname + filename, 'utf-8', function(err, content) {
        if (err) {
          onError(err);
          return;
        }
        onFileContent(filename, content);
      });
    });
  });
}

function now() {
	return new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
}

function ensureExists(path, mask, cb) {
    if (typeof mask == 'function') {
        cb = mask;
        mask = 0777;
    }

	mkdirp(path, err => {
	    if (err) {
	    	if (err.code == 'EEXIST') cb(null);
	    	else cb(err);
	    	console.error(err);
	    } else {
			cb(null);
	    }
	});
}

function getClassName(string) {
	string = camelCase(string);
	return capitalFirst(string);
}

function getClassNamePlural(string) {
	string = string.toLowerCase();
    if (config.plurals && config.plurals[string]) {
    	string = config.plurals[string];
    	return capitalFirst(string);
    } else {
    	return capitalFirst(string+"s");	
    }
}

function getSingularSlug(string, lang) {
	string = string.toLowerCase();
    if (config.singulars && config.singulars[string]) {
    	return config.singulars[string];
    } else {
    	if (string.slice(-1) == "s") {
	    	return string.slice(0, -1);
	    } else {
	    	return string;
	    }		
    }
}

function getPluralSlug(string, lang) {
	string = string.toLowerCase();
    if (config.plurals && config.plurals[string]) {
    	return config.plurals[string];
    } else {
    	return string + "s";	
    }
}

function capitalFirst(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

String.prototype.toCamelCase = function() {
    return this.replace(/^([A-Z])|\s(\w)/g, (match, p1, p2, offset) => {
        if (p2) return p2.toUpperCase();
        return p1.toLowerCase();        
    });
};

function isObject ( obj ) {
   return obj && (typeof obj  === "object");
}

function isArray ( obj ) { 
  return isObject(obj) && (obj instanceof Array);
}

var getType = function (elem) {
 	var _type =  Object.prototype.toString.call(elem).slice(8, -1);
  	if (_type == 'Number') {
		if (elem > (Math.pow(2,31)-1) && elem < (Math.pow(2,31)) ) {
			return "Double";
		} else {
			return "Integer";
		}
	} else {
		if (isArray(elem)) {
			return "Array";
		} else {
			return _type;
		}
	}
};

function getApiOptions(url, token, callback) {
	var options = {
		method: 'OPTIONS',
		json: true,
		url: url,
		headers: {
		  'Authorization':'Bearer ' + token
		}
	  };
	request(options, callback);
};

function typeToType(type, lang) {
	if (platform_data[lang].types[type] != undefined) {
		return platform_data[lang].types[type]
	} else {
		return platform_data[lang].types['__def__'];
	}
};

function saveFile(path, filename, content) {
	ensureExists(path, 0744, err => {
    	if (err) throw err;
    	fs.writeFile(path+"/"+filename, content, err => {
    		if (err) throw err;
		}); 
	});
};

start();



