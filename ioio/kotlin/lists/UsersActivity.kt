package mx.ioio.app.lists

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import org.jetbrains.anko.startActivity
import mx.ioio.app.BR
import mx.ioio.app.R
import mx.ioio.app.common.Adapter
import mx.ioio.app.common.ListActivity
import mx.ioio.app.databinding.ActivityListBinding
import mx.ioio.app.details.UserActivity
import mx.ioio.app.models.User

class UsersActivity : ListActivity() {

    lateinit var B: ActivityListBinding

    var items = ArrayList<User>()
    
    override val TAG = "UsersActivity"
    override var paginate = false
    override var plural = "users"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_list)
        backToolbar(B.include?.toolbar, R.string.users)
        prepareList(B.recyclerView, B.swipeRefresh,
                Adapter(R.layout.item_user, BR.user, items, itemClick)
        )
    }

    override fun load() {
        showWait()
        query(api().users()) {
            updateList(it, items)
        }
    }

    override fun detail(item: Int) {
        startActivity<UserActivity>(Gson().toJson(items[item]) to item_json)
    }
}
