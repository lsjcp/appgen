package mx.ioio.app.lists

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import org.jetbrains.anko.startActivity
import mx.ioio.app.BR
import mx.ioio.app.R
import mx.ioio.app.common.Adapter
import mx.ioio.app.common.ListActivity
import mx.ioio.app.databinding.ActivityListBinding
import mx.ioio.app.details.ItemActivity
import mx.ioio.app.models.Item

class ItemsActivity : ListActivity() {

    lateinit var B: ActivityListBinding

    var items = ArrayList<Item>()
    
    override val TAG = "ItemsActivity"
    override var paginate = false
    override var plural = "items"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_list)
        backToolbar(B.include?.toolbar, R.string.items)
        prepareList(B.recyclerView, B.swipeRefresh,
                Adapter(R.layout.item_item, BR.item, items, itemClick)
        )
    }

    override fun load() {
        showWait()
        query(api().items()) {
            updateList(it, items)
        }
    }

    override fun detail(item: Int) {
        startActivity<ItemActivity>(Gson().toJson(items[item]) to item_json)
    }
}
