package mx.ioio.app.lists

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import org.jetbrains.anko.startActivity
import mx.ioio.app.BR
import mx.ioio.app.R
import mx.ioio.app.common.Adapter
import mx.ioio.app.common.ListActivity
import mx.ioio.app.databinding.ActivityListBinding
import mx.ioio.app.details.BasketActivity
import mx.ioio.app.models.Basket

class BasketsActivity : ListActivity() {

    lateinit var B: ActivityListBinding

    var items = ArrayList<Basket>()
    
    override val TAG = "BasketsActivity"
    override var paginate = false
    override var plural = "baskets"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_list)
        backToolbar(B.include?.toolbar, R.string.baskets)
        prepareList(B.recyclerView, B.swipeRefresh,
                Adapter(R.layout.item_basket, BR.basket, items, itemClick)
        )
    }

    override fun load() {
        showWait()
        query(api().baskets()) {
            updateList(it, items)
        }
    }

    override fun detail(item: Int) {
        startActivity<BasketActivity>(Gson().toJson(items[item]) to item_json)
    }
}
