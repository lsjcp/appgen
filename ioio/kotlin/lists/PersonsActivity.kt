package mx.ioio.app.lists

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import org.jetbrains.anko.startActivity
import mx.ioio.app.BR
import mx.ioio.app.R
import mx.ioio.app.common.Adapter
import mx.ioio.app.common.ListActivity
import mx.ioio.app.databinding.ActivityListBinding
import mx.ioio.app.details.PersonActivity
import mx.ioio.app.models.Person

class PersonsActivity : ListActivity() {

    lateinit var B: ActivityListBinding

    var items = ArrayList<Person>()
    
    override val TAG = "PersonsActivity"
    override var paginate = false
    override var plural = "persons"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_list)
        backToolbar(B.include?.toolbar, R.string.persons)
        prepareList(B.recyclerView, B.swipeRefresh,
                Adapter(R.layout.item_person, BR.person, items, itemClick)
        )
    }

    override fun load() {
        showWait()
        query(api().persons()) {
            updateList(it, items)
        }
    }

    override fun detail(item: Int) {
        startActivity<PersonActivity>(Gson().toJson(items[item]) to item_json)
    }
}
