package mx.ioio.app.lists

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import org.jetbrains.anko.startActivity
import mx.ioio.app.BR
import mx.ioio.app.R
import mx.ioio.app.common.Adapter
import mx.ioio.app.common.ListActivity
import mx.ioio.app.databinding.ActivityListBinding
import mx.ioio.app.details.ReviewActivity
import mx.ioio.app.models.Review

class ReviewsActivity : ListActivity() {

    lateinit var B: ActivityListBinding

    var items = ArrayList<Review>()
    
    override val TAG = "ReviewsActivity"
    override var paginate = false
    override var plural = "reviews"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_list)
        backToolbar(B.include?.toolbar, R.string.reviews)
        prepareList(B.recyclerView, B.swipeRefresh,
                Adapter(R.layout.item_review, BR.review, items, itemClick)
        )
    }

    override fun load() {
        showWait()
        query(api().reviews()) {
            updateList(it, items)
        }
    }

    override fun detail(item: Int) {
        startActivity<ReviewActivity>(Gson().toJson(items[item]) to item_json)
    }
}
