package mx.ioio.app.lists

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import org.jetbrains.anko.startActivity
import mx.ioio.app.BR
import mx.ioio.app.R
import mx.ioio.app.common.Adapter
import mx.ioio.app.common.ListActivity
import mx.ioio.app.databinding.ActivityListBinding
import mx.ioio.app.details.CategoryActivity
import mx.ioio.app.models.Category

class CategoriesActivity : ListActivity() {

    lateinit var B: ActivityListBinding

    var items = ArrayList<Category>()
    
    override val TAG = "CategoriesActivity"
    override var paginate = false
    override var plural = "categories"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_list)
        backToolbar(B.include?.toolbar, R.string.categories)
        prepareList(B.recyclerView, B.swipeRefresh,
                Adapter(R.layout.item_category, BR.category, items, itemClick)
        )
    }

    override fun load() {
        showWait()
        query(api().categories()) {
            updateList(it, items)
        }
    }

    override fun detail(item: Int) {
        startActivity<CategoryActivity>(Gson().toJson(items[item]) to item_json)
    }
}
