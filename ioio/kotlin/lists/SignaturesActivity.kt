package mx.ioio.app.lists

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import org.jetbrains.anko.startActivity
import mx.ioio.app.BR
import mx.ioio.app.R
import mx.ioio.app.common.Adapter
import mx.ioio.app.common.ListActivity
import mx.ioio.app.databinding.ActivityListBinding
import mx.ioio.app.details.SignatureActivity
import mx.ioio.app.models.Signature

class SignaturesActivity : ListActivity() {

    lateinit var B: ActivityListBinding

    var items = ArrayList<Signature>()
    
    override val TAG = "SignaturesActivity"
    override var paginate = false
    override var plural = "signatures"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_list)
        backToolbar(B.include?.toolbar, R.string.signatures)
        prepareList(B.recyclerView, B.swipeRefresh,
                Adapter(R.layout.item_signature, BR.signature, items, itemClick)
        )
    }

    override fun load() {
        showWait()
        query(api().signatures()) {
            updateList(it, items)
        }
    }

    override fun detail(item: Int) {
        startActivity<SignatureActivity>(Gson().toJson(items[item]) to item_json)
    }
}
