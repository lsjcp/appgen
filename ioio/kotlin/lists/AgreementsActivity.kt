package mx.ioio.app.lists

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import org.jetbrains.anko.startActivity
import mx.ioio.app.BR
import mx.ioio.app.R
import mx.ioio.app.common.Adapter
import mx.ioio.app.common.ListActivity
import mx.ioio.app.databinding.ActivityListBinding
import mx.ioio.app.details.AgreementActivity
import mx.ioio.app.models.Agreement

class AgreementsActivity : ListActivity() {

    lateinit var B: ActivityListBinding

    var items = ArrayList<Agreement>()
    
    override val TAG = "AgreementsActivity"
    override var paginate = false
    override var plural = "agreements"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_list)
        backToolbar(B.include?.toolbar, R.string.agreements)
        prepareList(B.recyclerView, B.swipeRefresh,
                Adapter(R.layout.item_agreement, BR.agreement, items, itemClick)
        )
    }

    override fun load() {
        showWait()
        query(api().agreements()) {
            updateList(it, items)
        }
    }

    override fun detail(item: Int) {
        startActivity<AgreementActivity>(Gson().toJson(items[item]) to item_json)
    }
}
