package mx.ioio.app.lists

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import org.jetbrains.anko.startActivity
import mx.ioio.app.BR
import mx.ioio.app.R
import mx.ioio.app.common.Adapter
import mx.ioio.app.common.ListActivity
import mx.ioio.app.databinding.ActivityListBinding
import mx.ioio.app.details.PlaceActivity
import mx.ioio.app.models.Place

class PlacesActivity : ListActivity() {

    lateinit var B: ActivityListBinding

    var items = ArrayList<Place>()
    
    override val TAG = "PlacesActivity"
    override var paginate = false
    override var plural = "places"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_list)
        backToolbar(B.include?.toolbar, R.string.places)
        prepareList(B.recyclerView, B.swipeRefresh,
                Adapter(R.layout.item_place, BR.place, items, itemClick)
        )
    }

    override fun load() {
        showWait()
        query(api().places()) {
            updateList(it, items)
        }
    }

    override fun detail(item: Int) {
        startActivity<PlaceActivity>(Gson().toJson(items[item]) to item_json)
    }
}
