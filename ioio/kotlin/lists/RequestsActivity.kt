package mx.ioio.app.lists

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import org.jetbrains.anko.startActivity
import mx.ioio.app.BR
import mx.ioio.app.R
import mx.ioio.app.common.Adapter
import mx.ioio.app.common.ListActivity
import mx.ioio.app.databinding.ActivityListBinding
import mx.ioio.app.details.RequestActivity
import mx.ioio.app.models.Request

class RequestsActivity : ListActivity() {

    lateinit var B: ActivityListBinding

    var items = ArrayList<Request>()
    
    override val TAG = "RequestsActivity"
    override var paginate = false
    override var plural = "requests"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_list)
        backToolbar(B.include?.toolbar, R.string.requests)
        prepareList(B.recyclerView, B.swipeRefresh,
                Adapter(R.layout.item_request, BR.request, items, itemClick)
        )
    }

    override fun load() {
        showWait()
        query(api().requests()) {
            updateList(it, items)
        }
    }

    override fun detail(item: Int) {
        startActivity<RequestActivity>(Gson().toJson(items[item]) to item_json)
    }
}
