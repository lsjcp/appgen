package mx.ioio.app.lists

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import org.jetbrains.anko.startActivity
import mx.ioio.app.BR
import mx.ioio.app.R
import mx.ioio.app.common.Adapter
import mx.ioio.app.common.ListActivity
import mx.ioio.app.databinding.ActivityListBinding
import mx.ioio.app.details.OperationActivity
import mx.ioio.app.models.Operation

class OperationsActivity : ListActivity() {

    lateinit var B: ActivityListBinding

    var items = ArrayList<Operation>()
    
    override val TAG = "OperationsActivity"
    override var paginate = false
    override var plural = "operations"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = DataBindingUtil.setContentView(this, R.layout.activity_list)
        backToolbar(B.include?.toolbar, R.string.operations)
        prepareList(B.recyclerView, B.swipeRefresh,
                Adapter(R.layout.item_operation, BR.operation, items, itemClick)
        )
    }

    override fun load() {
        showWait()
        query(api().operations()) {
            updateList(it, items)
        }
    }

    override fun detail(item: Int) {
        startActivity<OperationActivity>(Gson().toJson(items[item]) to item_json)
    }
}
