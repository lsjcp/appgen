package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class User: Descriptable() {

    @SerializedName("first_firstName") var firstName: String? = null // Nombre
    @SerializedName("last_lastName") var lastName: String? = null // Apellidos
    var email: String? = null // Dirección de correo electrónico


}