package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class AgreementKind: Descriptable() {

    var slug: String? = null // Slug
    var country: CountryField? = null // Country
    var language: Language? = null // Language


}