package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Signature: Timeable() {

    var person: Person? = null // Person
    var agreement: Agreement? = null // Agreement
    @SerializedName("person_id") var personId: Int? = null // Person id
    @SerializedName("agreement_id") var agreementId: Int? = null // Agreement id
    var hash: String? = null // Hash
    var role: String? = null // Role


}