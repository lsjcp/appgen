package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Basket: Descriptable() {

    var items: String? = null // Items
    var owner: Owner? = null // Owner
    @SerializedName("owner_id") var ownerId: Int? = null // Owner id
    @SerializedName("items_add") var itemsAdd: Int? = null // Items add
    @SerializedName("items_remove") var itemsRemove: Int? = null // Items remove
    var kind: String? = null // Kind
    var public: Boolean? = null // Public


}