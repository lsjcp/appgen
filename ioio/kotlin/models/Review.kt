package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Review: Listable() {

    var reviewer: Reviewer? = null // Reviewer
    @SerializedName("reviewer_id") var reviewerId: Int? = null // Reviewer id
    var target: String? = null // Target
    var comment: String? = null // Comment
    var rate: Int? = null // Rate
    var like: Boolean? = null // Like
    var moderation: String? = null // Moderation
    var reviewed: String? = null // Reviewed


}