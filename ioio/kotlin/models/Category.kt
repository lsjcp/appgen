package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Category: Descriptable() {

    var order: Int? = null // Order
    var keywords: String? = null // Keywords
    var parent: String? = null // Parent


}