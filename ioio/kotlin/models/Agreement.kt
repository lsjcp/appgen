package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Agreement: File() {

    var kind: Kind? = null // Kind
    @SerializedName("kind_id") var kindId: Int? = null // Kind id
    var hash: String? = null // Hash
    var version: Int? = null // Version


}