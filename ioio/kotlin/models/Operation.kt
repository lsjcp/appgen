package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Operation: Timeable() {

    var landlord: Landlord? = null // Landlord
    var tenant: Tenant? = null // Tenant
    var item: Item? = null // Item
    var agreement: Agreement? = null // Agreement
    var nda: Nda? = null // Nda
    var place: Place? = null // Place
    @SerializedName("landlord_id") var landlordId: Int? = null // Landlord id
    @SerializedName("tenant_id") var tenantId: Int? = null // Tenant id
    @SerializedName("item_id") var itemId: Int? = null // Item id
    @SerializedName("agreement_id") var agreementId: Int? = null // Agreement id
    @SerializedName("nda_id") var ndaId: Int? = null // Nda id
    @SerializedName("place_id") var placeId: Int? = null // Place id
    var kind: String? = null // Kind
    var iterations: Int? = null // Iterations
    var iteration: Int? = null // Iteration
    var recurrent: Boolean? = null // Recurrent
    var renewable: Boolean? = null // Renewable
    var partial: Boolean? = null // Partial
    var transfer: String? = null // Transfer
    var release: String? = null // Release


}