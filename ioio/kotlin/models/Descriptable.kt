package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Descriptable() {

    var id: Int? = null // Id
    var name: String? = null // Title
    var title: String? = null // Title
    var description: String? = null // Description
    var image: String? = null // Image
    var metadata: String? = null // Metadata
    var enabled: Boolean = True // Enabled
    var display: Boolean = True // Enabled


}