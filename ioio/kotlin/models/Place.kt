package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Place: Descriptable() {

    var country: String? = null // Country
    var lat: String? = null // Lat
    var lon: String? = null // Lon
    var address: String? = null // Address
    var identifier: String? = null // Identifier
    var state: String? = null // State


}