package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class List: Descriptable() {

    var owner: Owner? = null // Owner
    var item: Item? = null // Item
    @SerializedName("owner_id") var ownerId: Int? = null // Owner id
    @SerializedName("item_id") var itemId: Int? = null // Item id
    var name: String? = null // Name
    var kind: String? = null // Kind
    var public: Boolean? = null // Public
    var items: String? = null // Items


}