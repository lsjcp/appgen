package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Request: Timeable() {

    var candidate: Candidate? = null // Candidate
    var item: Item? = null // Item
    var place: Place? = null // Place
    @SerializedName("candidate_id") var candidateId: Int? = null // Candidate id
    @SerializedName("item_id") var itemId: Int? = null // Item id
    var comment: String? = null // Comment
    var acceptance: String? = null // Acceptance


}