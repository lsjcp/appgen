package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Person: Descriptable() {

    var user: User? = null // User
    var languages: Languages? = null // Languages
    var country: String? = null // Country
    var gender: String? = null // Gender
    var birthday: String? = null // Birthday
    var location: String? = null // Location
    var key: String? = null // Key
    var activity: String? = null // Activity


}