package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Media: File() {

    var language: Language? = null // Language
    var preview: String? = null // Preview
    var screenshot: String? = null // Screenshot
    var moderation: String? = null // Moderation


}