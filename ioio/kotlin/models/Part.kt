package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Part: TimeableListable() {

    @SerializedName("content_type") var contentString?: String? = null // Content Type
    var link: String? = null // Link
    @SerializedName("item_layout") var itemLayout: String? = null // Item Layout
    var region: String? = null // Region
    var tags: String? = null // Tags
    var languages: String? = null // Languages
    var refresh: Int? = 0 // Refresh


}