package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Timeable: Descriptable() {

    var zone: String? = null // Zone
    var creation: DateTime? = null // Creation
    var expiration: DateTime? = null // Expiration
    var start: DateTime? = null // Start
    var end: DateTime? = null // End
    var update: DateTime? = null // Update
    var status: String? = null // Status


}