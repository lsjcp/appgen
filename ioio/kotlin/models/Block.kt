package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Block: Part() {

    var items: String? = null // Items
    var contentString?: String? = null // ContentType
    var itemLayout: String? = null // ItemLayout
    var groupLayout: String? = null // GroupLayout


}