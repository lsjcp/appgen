package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class Item: TimeableListable() {

    var slug: String? = null // Slug
    var media: String? = null // Media
    var owner: Owner? = null // Owner
    @SerializedName("owner_id") var ownerId: Int? = null // Owner id
    var category: Category? = null // Category
    @SerializedName("category_id") var categoryId: Int? = null // Category id
    var partitive: Boolean? = null // Partitive
    var consumable: Boolean? = null // Consumable
    var abstract: Boolean? = null // Abstract
    var price: String? = null // Price
    var value: String? = null // Value
    var valuation: String? = null // Valuation
    var currency: String? = null // Currency
    var frame: String? = null // Frame
    var audience: String? = null // Audience
    var outreach: String? = null // Outreach
    var kind: String? = null // Kind
    var location: String? = null // Location
    var keywords: String? = null // Keywords
    var brand: String? = null // Brand


}