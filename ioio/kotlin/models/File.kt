package mx.ioio.app.models

import com.google.gson.annotations.SerializedName
import org.joda.time.*

open class File: Listable() {

    var uri: String? = null // URI
    var mime: String? = null // Mime


}