package mx.ioio.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.DetailActivity
import mx.ioio.app.databinding.ActivitySignatureBinding
import mx.ioio.app.models.Signature
import com.google.gson.Gson

class SignatureActivity : DetailActivity() {

    lateinit var B: ActivitySignatureBinding

    override val TAG = "SignatureActivity"
    override var model = "signature"

    val className = Signature::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_signature)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.signature)
        load()
    }

    override fun load() {
        print("loading signature")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.signature = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().signature(id)) {
                B.signature = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.signature?.let { signature ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

