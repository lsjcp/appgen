package mx.ioio.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.DetailActivity
import mx.ioio.app.databinding.ActivityPlaceBinding
import mx.ioio.app.models.Place
import com.google.gson.Gson

class PlaceActivity : DetailActivity() {

    lateinit var B: ActivityPlaceBinding

    override val TAG = "PlaceActivity"
    override var model = "place"

    val className = Place::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_place)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.place)
        load()
    }

    override fun load() {
        print("loading place")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.place = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().place(id)) {
                B.place = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.place?.let { place ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

