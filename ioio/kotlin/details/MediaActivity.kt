package mx.ioio.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.DetailActivity
import mx.ioio.app.databinding.ActivityMediaBinding
import mx.ioio.app.models.Media
import com.google.gson.Gson

class MediaActivity : DetailActivity() {

    lateinit var B: ActivityMediaBinding

    override val TAG = "MediaActivity"
    override var model = "media"

    val className = Media::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_media)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.media)
        load()
    }

    override fun load() {
        print("loading media")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.media = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().media(id)) {
                B.media = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.media?.let { media ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

