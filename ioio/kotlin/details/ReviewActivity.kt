package mx.ioio.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.DetailActivity
import mx.ioio.app.databinding.ActivityReviewBinding
import mx.ioio.app.models.Review
import com.google.gson.Gson

class ReviewActivity : DetailActivity() {

    lateinit var B: ActivityReviewBinding

    override val TAG = "ReviewActivity"
    override var model = "review"

    val className = Review::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_review)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.review)
        load()
    }

    override fun load() {
        print("loading review")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.review = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().review(id)) {
                B.review = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.review?.let { review ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

