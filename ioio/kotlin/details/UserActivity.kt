package mx.ioio.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.DetailActivity
import mx.ioio.app.databinding.ActivityUserBinding
import mx.ioio.app.models.User
import com.google.gson.Gson

class UserActivity : DetailActivity() {

    lateinit var B: ActivityUserBinding

    override val TAG = "UserActivity"
    override var model = "user"

    val className = User::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_user)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.user)
        load()
    }

    override fun load() {
        print("loading user")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.user = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().user(id)) {
                B.user = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.user?.let { user ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

