package mx.ioio.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.DetailActivity
import mx.ioio.app.databinding.ActivityBlockBinding
import mx.ioio.app.models.Block
import com.google.gson.Gson

class BlockActivity : DetailActivity() {

    lateinit var B: ActivityBlockBinding

    override val TAG = "BlockActivity"
    override var model = "block"

    val className = Block::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_block)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.block)
        load()
    }

    override fun load() {
        print("loading block")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.block = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().block(id)) {
                B.block = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.block?.let { block ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

