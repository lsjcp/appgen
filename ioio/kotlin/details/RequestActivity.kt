package mx.ioio.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.DetailActivity
import mx.ioio.app.databinding.ActivityRequestBinding
import mx.ioio.app.models.Request
import com.google.gson.Gson

class RequestActivity : DetailActivity() {

    lateinit var B: ActivityRequestBinding

    override val TAG = "RequestActivity"
    override var model = "request"

    val className = Request::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_request)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.request)
        load()
    }

    override fun load() {
        print("loading request")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.request = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().request(id)) {
                B.request = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.request?.let { request ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

