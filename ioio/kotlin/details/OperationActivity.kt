package mx.ioio.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.DetailActivity
import mx.ioio.app.databinding.ActivityOperationBinding
import mx.ioio.app.models.Operation
import com.google.gson.Gson

class OperationActivity : DetailActivity() {

    lateinit var B: ActivityOperationBinding

    override val TAG = "OperationActivity"
    override var model = "operation"

    val className = Operation::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_operation)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.operation)
        load()
    }

    override fun load() {
        print("loading operation")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.operation = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().operation(id)) {
                B.operation = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.operation?.let { operation ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

