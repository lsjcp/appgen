package mx.ioio.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.DetailActivity
import mx.ioio.app.databinding.ActivityItemBinding
import mx.ioio.app.models.Item
import com.google.gson.Gson

class ItemActivity : DetailActivity() {

    lateinit var B: ActivityItemBinding

    override val TAG = "ItemActivity"
    override var model = "item"

    val className = Item::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_item)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.item)
        load()
    }

    override fun load() {
        print("loading item")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.item = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().item(id)) {
                B.item = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.item?.let { item ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

