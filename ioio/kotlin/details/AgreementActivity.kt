package mx.ioio.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.DetailActivity
import mx.ioio.app.databinding.ActivityAgreementBinding
import mx.ioio.app.models.Agreement
import com.google.gson.Gson

class AgreementActivity : DetailActivity() {

    lateinit var B: ActivityAgreementBinding

    override val TAG = "AgreementActivity"
    override var model = "agreement"

    val className = Agreement::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_agreement)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.agreement)
        load()
    }

    override fun load() {
        print("loading agreement")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.agreement = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().agreement(id)) {
                B.agreement = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.agreement?.let { agreement ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

