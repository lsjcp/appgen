package mx.ioio.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.DetailActivity
import mx.ioio.app.databinding.ActivityPersonBinding
import mx.ioio.app.models.Person
import com.google.gson.Gson

class PersonActivity : DetailActivity() {

    lateinit var B: ActivityPersonBinding

    override val TAG = "PersonActivity"
    override var model = "person"

    val className = Person::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_person)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.person)
        load()
    }

    override fun load() {
        print("loading person")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.person = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().person(id)) {
                B.person = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.person?.let { person ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

