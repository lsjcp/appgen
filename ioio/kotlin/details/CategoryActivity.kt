package mx.ioio.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.DetailActivity
import mx.ioio.app.databinding.ActivityCategoryBinding
import mx.ioio.app.models.Category
import com.google.gson.Gson

class CategoryActivity : DetailActivity() {

    lateinit var B: ActivityCategoryBinding

    override val TAG = "CategoryActivity"
    override var model = "category"

    val className = Category::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_category)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.category)
        load()
    }

    override fun load() {
        print("loading category")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.category = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().category(id)) {
                B.category = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.category?.let { category ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

