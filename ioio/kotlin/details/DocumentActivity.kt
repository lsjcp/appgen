package mx.ioio.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.DetailActivity
import mx.ioio.app.databinding.ActivityDocumentBinding
import mx.ioio.app.models.Document
import com.google.gson.Gson

class DocumentActivity : DetailActivity() {

    lateinit var B: ActivityDocumentBinding

    override val TAG = "DocumentActivity"
    override var model = "document"

    val className = Document::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_document)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.document)
        load()
    }

    override fun load() {
        print("loading document")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.document = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().document(id)) {
                B.document = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.document?.let { document ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

