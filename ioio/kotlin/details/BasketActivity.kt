package mx.ioio.app.details

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.DetailActivity
import mx.ioio.app.databinding.ActivityBasketBinding
import mx.ioio.app.models.Basket
import com.google.gson.Gson

class BasketActivity : DetailActivity() {

    lateinit var B: ActivityBasketBinding

    override val TAG = "BasketActivity"
    override var model = "basket"

    val className = Basket::class.java
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_basket)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.basket)
        load()
    }

    override fun load() {
        print("loading basket")
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.basket = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            id = it
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().basket(id)) {
                B.basket = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.basket?.let { basket ->
            //TODO: After load actionns
        }
    }

    override fun edit(view: View?) {
        print("goto edit")
    }
}

