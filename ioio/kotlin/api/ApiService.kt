package mx.ioio.app.api

import io.reactivex.Flowable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import retrofit2.http.Body
import mx.ioio.app.models.*

interface ApiService {

    
    //User
	
    @GET("users")
	fun users(): Flowable<ArrayList<User>>

	@GET("users/{id}")
	fun user(@Path("id") id: Int): Flowable<User>

	@PATCH("users/{id}")
	fun editUser(@Path("id") id: Int, @Body user: User): Flowable<User>

	@POST("users")
	fun createUser(@Body user: User): Flowable<User>

	@DELETE("users/{id}")
	fun deleteUser(@Path("id") id: Int): Flowable<Any>
    
    

    //Category
	
    @GET("categories")
	fun categories(): Flowable<ArrayList<Category>>

	@GET("categories/{id}")
	fun category(@Path("id") id: Int): Flowable<Category>

	@PATCH("categories/{id}")
	fun editCategory(@Path("id") id: Int, @Body category: Category): Flowable<Category>

	@POST("categories")
	fun createCategory(@Body category: Category): Flowable<Category>

	@DELETE("categories/{id}")
	fun deleteCategory(@Path("id") id: Int): Flowable<Any>
    
    

    //Person
	
    @GET("persons")
	fun persons(): Flowable<ArrayList<Person>>

	@GET("persons/{id}")
	fun person(@Path("id") id: Int): Flowable<Person>

	@PATCH("persons/{id}")
	fun editPerson(@Path("id") id: Int, @Body person: Person): Flowable<Person>

	@POST("persons")
	fun createPerson(@Body person: Person): Flowable<Person>

	@DELETE("persons/{id}")
	fun deletePerson(@Path("id") id: Int): Flowable<Any>
    
    

    //Item
	
    @GET("items")
	fun items(): Flowable<ArrayList<Item>>

	@GET("items/{id}")
	fun item(@Path("id") id: Int): Flowable<Item>

	@PATCH("items/{id}")
	fun editItem(@Path("id") id: Int, @Body item: Item): Flowable<Item>

	@POST("items")
	fun createItem(@Body item: Item): Flowable<Item>

	@DELETE("items/{id}")
	fun deleteItem(@Path("id") id: Int): Flowable<Any>
    
    

    //Request
	
    @GET("requests")
	fun requests(): Flowable<ArrayList<Request>>

	@GET("requests/{id}")
	fun request(@Path("id") id: Int): Flowable<Request>

	@PATCH("requests/{id}")
	fun editRequest(@Path("id") id: Int, @Body request: Request): Flowable<Request>

	@POST("requests")
	fun createRequest(@Body request: Request): Flowable<Request>

	@DELETE("requests/{id}")
	fun deleteRequest(@Path("id") id: Int): Flowable<Any>
    
    

    //Operation
	
    @GET("operations")
	fun operations(): Flowable<ArrayList<Operation>>

	@GET("operations/{id}")
	fun operation(@Path("id") id: Int): Flowable<Operation>

	@PATCH("operations/{id}")
	fun editOperation(@Path("id") id: Int, @Body operation: Operation): Flowable<Operation>

	@POST("operations")
	fun createOperation(@Body operation: Operation): Flowable<Operation>

	@DELETE("operations/{id}")
	fun deleteOperation(@Path("id") id: Int): Flowable<Any>
    
    

    //Review
	
    @GET("reviews")
	fun reviews(): Flowable<ArrayList<Review>>

	@GET("reviews/{id}")
	fun review(@Path("id") id: Int): Flowable<Review>

	@PATCH("reviews/{id}")
	fun editReview(@Path("id") id: Int, @Body review: Review): Flowable<Review>

	@POST("reviews")
	fun createReview(@Body review: Review): Flowable<Review>

	@DELETE("reviews/{id}")
	fun deleteReview(@Path("id") id: Int): Flowable<Any>
    
    

    //Basket
	
    @GET("baskets")
	fun baskets(): Flowable<ArrayList<Basket>>

	@GET("baskets/{id}")
	fun basket(@Path("id") id: Int): Flowable<Basket>

	@PATCH("baskets/{id}")
	fun editBasket(@Path("id") id: Int, @Body basket: Basket): Flowable<Basket>

	@POST("baskets")
	fun createBasket(@Body basket: Basket): Flowable<Basket>

	@DELETE("baskets/{id}")
	fun deleteBasket(@Path("id") id: Int): Flowable<Any>
    
    

    //Place
	
    @GET("places")
	fun places(): Flowable<ArrayList<Place>>

	@GET("places/{id}")
	fun place(@Path("id") id: Int): Flowable<Place>

	@PATCH("places/{id}")
	fun editPlace(@Path("id") id: Int, @Body place: Place): Flowable<Place>

	@POST("places")
	fun createPlace(@Body place: Place): Flowable<Place>

	@DELETE("places/{id}")
	fun deletePlace(@Path("id") id: Int): Flowable<Any>
    
    

    //Document
	
    @GET("documents")
	fun documents(): Flowable<ArrayList<Document>>

	@GET("documents/{id}")
	fun document(@Path("id") id: Int): Flowable<Document>

	@PATCH("documents/{id}")
	fun editDocument(@Path("id") id: Int, @Body document: Document): Flowable<Document>

	@POST("documents")
	fun createDocument(@Body document: Document): Flowable<Document>

	@DELETE("documents/{id}")
	fun deleteDocument(@Path("id") id: Int): Flowable<Any>
    
    

    //Agreement
	
    @GET("agreements")
	fun agreements(): Flowable<ArrayList<Agreement>>

	@GET("agreements/{id}")
	fun agreement(@Path("id") id: Int): Flowable<Agreement>

	@PATCH("agreements/{id}")
	fun editAgreement(@Path("id") id: Int, @Body agreement: Agreement): Flowable<Agreement>

	@POST("agreements")
	fun createAgreement(@Body agreement: Agreement): Flowable<Agreement>

	@DELETE("agreements/{id}")
	fun deleteAgreement(@Path("id") id: Int): Flowable<Any>
    
    

    //Signature
	
    @GET("signatures")
	fun signatures(): Flowable<ArrayList<Signature>>

	@GET("signatures/{id}")
	fun signature(@Path("id") id: Int): Flowable<Signature>

	@PATCH("signatures/{id}")
	fun editSignature(@Path("id") id: Int, @Body signature: Signature): Flowable<Signature>

	@POST("signatures")
	fun createSignature(@Body signature: Signature): Flowable<Signature>

	@DELETE("signatures/{id}")
	fun deleteSignature(@Path("id") id: Int): Flowable<Any>
    
    

    //Media
	
    @GET("medium")
	fun medium(): Flowable<ArrayList<Media>>

	@GET("medium/{id}")
	fun media(@Path("id") id: Int): Flowable<Media>

	@PATCH("medium/{id}")
	fun editMedia(@Path("id") id: Int, @Body media: Media): Flowable<Media>

	@POST("medium")
	fun createMedia(@Body media: Media): Flowable<Media>

	@DELETE("medium/{id}")
	fun deleteMedia(@Path("id") id: Int): Flowable<Any>
    
    

    //Block
	
    @GET("blocks")
	fun blocks(): Flowable<ArrayList<Block>>

	@GET("blocks/{id}")
	fun block(@Path("id") id: Int): Flowable<Block>

	@PATCH("blocks/{id}")
	fun editBlock(@Path("id") id: Int, @Body block: Block): Flowable<Block>

	@POST("blocks")
	fun createBlock(@Body block: Block): Flowable<Block>

	@DELETE("blocks/{id}")
	fun deleteBlock(@Path("id") id: Int): Flowable<Any>
    
    

    //Post
	
    @GET("posts")
	fun posts(): Flowable<ArrayList<Post>>

	@GET("posts/{id}")
	fun post(@Path("id") id: Int): Flowable<Post>

	@PATCH("posts/{id}")
	fun editPost(@Path("id") id: Int, @Body post: Post): Flowable<Post>

	@POST("posts")
	fun createPost(@Body post: Post): Flowable<Post>

	@DELETE("posts/{id}")
	fun deletePost(@Path("id") id: Int): Flowable<Any>
    
    

    

}