package mx.ioio.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.EditActivity
import mx.ioio.app.databinding.ActivityCategoryEditBinding
import mx.ioio.app.models.Category
import com.google.gson.Gson

class CategoryEditActivity : EditActivity() {

    lateinit var B: ActivityCategoryEditBinding

    override val TAG = "CategoryEditActivity"
    override var model = "category"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = Category::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_category_edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.category)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.category = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().category(id)) {
                B.category = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.category?.let { category ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating category")
        var valid = true

        //Field Validation
            
		//order validation
        val order = B.inputOrder.text.toString()
        if (order.isNullOrBlank()) {
            valid = false
            throwField(B.inputOrder, "EMPTY")
        }
		
    
		//keywords validation
        val keywords = B.inputKeywords.text.toString()
        if (keywords.isNullOrBlank()) {
            valid = false
            throwField(B.inputKeywords, "EMPTY")
        }
		
    
		//parent validation
        val parent = B.inputParent.text.toString()
        if (parent.isNullOrBlank()) {
            valid = false
            throwField(B.inputParent, "EMPTY")
        }
		


        return valid;
    }

    override fun edit() {
        if (B.category == null) return
        print("editing category")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        

        //Edit Field
            
		B.category?.order = B.inputOrder.text.toString().toIntOrNull()
		
    
		B.category?.keywords = B.inputKeywords.text.toString()
		
    
		B.category?.parent = B.inputParent.text.toString()
		


        val category = B.category ?: return
        val id = category.id ?: return
        mutate(api().editCategory(id, category)) { 
            B.category = it
            loaded()
        }
    }

    override fun create() {
        print("creating category")
        showWait()
        
        B.category = Category()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
            
		B.category?.order = B.inputOrder.text.toString().toIntOrNull()
		
    
		B.category?.keywords = B.inputKeywords.text.toString()
		
    
		B.category?.parent = B.inputParent.text.toString()
		


        val category = B.category ?: return
        mutate(api().createCategory(category)) { 
            B.category = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.category?.id ?: return
        showWait()
        print("deleting category")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().deleteCategory(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

