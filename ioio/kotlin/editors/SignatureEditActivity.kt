package mx.ioio.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.EditActivity
import mx.ioio.app.databinding.ActivitySignatureEditBinding
import mx.ioio.app.models.Signature
import com.google.gson.Gson

class SignatureEditActivity : EditActivity() {

    lateinit var B: ActivitySignatureEditBinding

    override val TAG = "SignatureEditActivity"
    override var model = "signature"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = Signature::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_signature_edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.signature)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.signature = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().signature(id)) {
                B.signature = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.signature?.let { signature ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating signature")
        var valid = true

        //Field Validation
            
		//person_id validation
        val personId = B.inputPersonId.text.toString()
        if (personId.isNullOrBlank()) {
            valid = false
            throwField(B.inputPersonId, "EMPTY")
        }
		
    
		//agreement_id validation
        val agreementId = B.inputAgreementId.text.toString()
        if (agreementId.isNullOrBlank()) {
            valid = false
            throwField(B.inputAgreementId, "EMPTY")
        }
		
    
		//hash validation
        val hash = B.inputHash.text.toString()
        if (hash.isNullOrBlank()) {
            valid = false
            throwField(B.inputHash, "EMPTY")
        }
		
    
		//role validation
        val role = B.inputRole.text.toString()
        if (role.isNullOrBlank()) {
            valid = false
            throwField(B.inputRole, "EMPTY")
        }
		


        return valid;
    }

    override fun edit() {
        if (B.signature == null) return
        print("editing signature")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        undefined
        undefined
        undefined
        undefined

        //Edit Field
            
		B.signature?.personId = B.inputPersonId.text.toString().toIntOrNull()
		
    
		B.signature?.agreementId = B.inputAgreementId.text.toString().toIntOrNull()
		
    
		B.signature?.hash = B.inputHash.text.toString()
		
    
		B.signature?.role = B.inputRole.text.toString()
		


        val signature = B.signature ?: return
        val id = signature.id ?: return
        mutate(api().editSignature(id, signature)) { 
            B.signature = it
            loaded()
        }
    }

    override fun create() {
        print("creating signature")
        showWait()
        
        B.signature = Signature()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
            
		B.signature?.personId = B.inputPersonId.text.toString().toIntOrNull()
		
    
		B.signature?.agreementId = B.inputAgreementId.text.toString().toIntOrNull()
		
    
		B.signature?.hash = B.inputHash.text.toString()
		
    
		B.signature?.role = B.inputRole.text.toString()
		


        val signature = B.signature ?: return
        mutate(api().createSignature(signature)) { 
            B.signature = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.signature?.id ?: return
        showWait()
        print("deleting signature")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().deleteSignature(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

