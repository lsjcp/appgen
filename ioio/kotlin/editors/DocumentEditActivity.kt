package mx.ioio.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.EditActivity
import mx.ioio.app.databinding.ActivityDocumentEditBinding
import mx.ioio.app.models.Document
import com.google.gson.Gson

class DocumentEditActivity : EditActivity() {

    lateinit var B: ActivityDocumentEditBinding

    override val TAG = "DocumentEditActivity"
    override var model = "document"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = Document::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_document_edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.document)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.document = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().document(id)) {
                B.document = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.document?.let { document ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating document")
        var valid = true

        //Field Validation
            
		//kind_id validation
        val kindId = B.inputKindId.text.toString()
        if (kindId.isNullOrBlank()) {
            valid = false
            throwField(B.inputKindId, "EMPTY")
        }
		
    
		//hash validation
        val hash = B.inputHash.text.toString()
        if (hash.isNullOrBlank()) {
            valid = false
            throwField(B.inputHash, "EMPTY")
        }
		
    
		//version validation
        val version = B.inputVersion.text.toString()
        if (version.isNullOrBlank()) {
            valid = false
            throwField(B.inputVersion, "EMPTY")
        }
		


        return valid;
    }

    override fun edit() {
        if (B.document == null) return
        print("editing document")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        undefined
        undefined
        undefined
        undefined

        //Edit Field
            
		B.document?.kindId = B.inputKindId.text.toString().toIntOrNull()
		
    
		B.document?.hash = B.inputHash.text.toString()
		
    
		B.document?.version = B.inputVersion.text.toString().toIntOrNull()
		


        val document = B.document ?: return
        val id = document.id ?: return
        mutate(api().editDocument(id, document)) { 
            B.document = it
            loaded()
        }
    }

    override fun create() {
        print("creating document")
        showWait()
        
        B.document = Document()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
            
		B.document?.kindId = B.inputKindId.text.toString().toIntOrNull()
		
    
		B.document?.hash = B.inputHash.text.toString()
		
    
		B.document?.version = B.inputVersion.text.toString().toIntOrNull()
		


        val document = B.document ?: return
        mutate(api().createDocument(document)) { 
            B.document = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.document?.id ?: return
        showWait()
        print("deleting document")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().deleteDocument(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

