package mx.ioio.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.EditActivity
import mx.ioio.app.databinding.ActivityRequestEditBinding
import mx.ioio.app.models.Request
import com.google.gson.Gson

class RequestEditActivity : EditActivity() {

    lateinit var B: ActivityRequestEditBinding

    override val TAG = "RequestEditActivity"
    override var model = "request"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = Request::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_request_edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.request)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.request = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().request(id)) {
                B.request = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.request?.let { request ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating request")
        var valid = true

        //Field Validation
            
		//candidate_id validation
        val candidateId = B.inputCandidateId.text.toString()
        if (candidateId.isNullOrBlank()) {
            valid = false
            throwField(B.inputCandidateId, "EMPTY")
        }
		
    
		//item_id validation
        val itemId = B.inputItemId.text.toString()
        if (itemId.isNullOrBlank()) {
            valid = false
            throwField(B.inputItemId, "EMPTY")
        }
		
    
		//comment validation
        val comment = B.inputComment.text.toString()
        if (comment.isNullOrBlank()) {
            valid = false
            throwField(B.inputComment, "EMPTY")
        }
		
    
		//acceptance validation
        val acceptance = B.inputAcceptance.text.toString()
        if (acceptance.isNullOrBlank()) {
            valid = false
            throwField(B.inputAcceptance, "EMPTY")
        }
		


        return valid;
    }

    override fun edit() {
        if (B.request == null) return
        print("editing request")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        

        //Edit Field
            
		B.request?.candidateId = B.inputCandidateId.text.toString().toIntOrNull()
		
    
		B.request?.itemId = B.inputItemId.text.toString().toIntOrNull()
		
    
		B.request?.comment = B.inputComment.text.toString()
		
    
		B.request?.acceptance = B.inputAcceptance.text.toString()
		


        val request = B.request ?: return
        val id = request.id ?: return
        mutate(api().editRequest(id, request)) { 
            B.request = it
            loaded()
        }
    }

    override fun create() {
        print("creating request")
        showWait()
        
        B.request = Request()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
            
		B.request?.candidateId = B.inputCandidateId.text.toString().toIntOrNull()
		
    
		B.request?.itemId = B.inputItemId.text.toString().toIntOrNull()
		
    
		B.request?.comment = B.inputComment.text.toString()
		
    
		B.request?.acceptance = B.inputAcceptance.text.toString()
		


        val request = B.request ?: return
        mutate(api().createRequest(request)) { 
            B.request = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.request?.id ?: return
        showWait()
        print("deleting request")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().deleteRequest(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

