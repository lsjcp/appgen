package mx.ioio.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.EditActivity
import mx.ioio.app.databinding.ActivityBasketEditBinding
import mx.ioio.app.models.Basket
import com.google.gson.Gson

class BasketEditActivity : EditActivity() {

    lateinit var B: ActivityBasketEditBinding

    override val TAG = "BasketEditActivity"
    override var model = "basket"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = Basket::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_basket_edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.basket)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.basket = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().basket(id)) {
                B.basket = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.basket?.let { basket ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating basket")
        var valid = true

        //Field Validation
            
		//items validation
        val items = B.inputItems.text.toString()
        if (items.isNullOrBlank()) {
            valid = false
            throwField(B.inputItems, "EMPTY")
        }
		
    
		//owner_id validation
        val ownerId = B.inputOwnerId.text.toString()
        if (ownerId.isNullOrBlank()) {
            valid = false
            throwField(B.inputOwnerId, "EMPTY")
        }
		
    
		//items_add validation
        val itemsAdd = B.inputItemsAdd.text.toString()
        if (itemsAdd.isNullOrBlank()) {
            valid = false
            throwField(B.inputItemsAdd, "EMPTY")
        }
		
    
		//items_remove validation
        val itemsRemove = B.inputItemsRemove.text.toString()
        if (itemsRemove.isNullOrBlank()) {
            valid = false
            throwField(B.inputItemsRemove, "EMPTY")
        }
		
    
		//kind validation
        val kind = B.inputKind.text.toString()
        if (kind.isNullOrBlank()) {
            valid = false
            throwField(B.inputKind, "EMPTY")
        }
		


        return valid;
    }

    override fun edit() {
        if (B.basket == null) return
        print("editing basket")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        

        //Edit Field
            
		B.basket?.items = B.inputItems.text.toString()
		
    
		B.basket?.ownerId = B.inputOwnerId.text.toString().toIntOrNull()
		
    
		B.basket?.itemsAdd = B.inputItemsAdd.text.toString().toIntOrNull()
		
    
		B.basket?.itemsRemove = B.inputItemsRemove.text.toString().toIntOrNull()
		
    
		B.basket?.kind = B.inputKind.text.toString()
		


        val basket = B.basket ?: return
        val id = basket.id ?: return
        mutate(api().editBasket(id, basket)) { 
            B.basket = it
            loaded()
        }
    }

    override fun create() {
        print("creating basket")
        showWait()
        
        B.basket = Basket()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
            
		B.basket?.items = B.inputItems.text.toString()
		
    
		B.basket?.ownerId = B.inputOwnerId.text.toString().toIntOrNull()
		
    
		B.basket?.itemsAdd = B.inputItemsAdd.text.toString().toIntOrNull()
		
    
		B.basket?.itemsRemove = B.inputItemsRemove.text.toString().toIntOrNull()
		
    
		B.basket?.kind = B.inputKind.text.toString()
		


        val basket = B.basket ?: return
        mutate(api().createBasket(basket)) { 
            B.basket = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.basket?.id ?: return
        showWait()
        print("deleting basket")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().deleteBasket(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

