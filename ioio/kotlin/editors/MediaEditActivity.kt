package mx.ioio.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.EditActivity
import mx.ioio.app.databinding.ActivityMediaEditBinding
import mx.ioio.app.models.Media
import com.google.gson.Gson

class MediaEditActivity : EditActivity() {

    lateinit var B: ActivityMediaEditBinding

    override val TAG = "MediaEditActivity"
    override var model = "media"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = Media::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_media_edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.media)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.media = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().media(id)) {
                B.media = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.media?.let { media ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating media")
        var valid = true

        //Field Validation
            
		//preview validation
        val preview = B.inputPreview.text.toString()
        if (preview.isNullOrBlank()) {
            valid = false
            throwField(B.inputPreview, "EMPTY")
        }
		
    
		//screenshot validation
        val screenshot = B.inputScreenshot.text.toString()
        if (screenshot.isNullOrBlank()) {
            valid = false
            throwField(B.inputScreenshot, "EMPTY")
        }
		
    
		//moderation validation
        val moderation = B.inputModeration.text.toString()
        if (moderation.isNullOrBlank()) {
            valid = false
            throwField(B.inputModeration, "EMPTY")
        }
		


        return valid;
    }

    override fun edit() {
        if (B.media == null) return
        print("editing media")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        

        //Edit Field
            
		B.media?.preview = B.inputPreview.text.toString()
		
    
		B.media?.screenshot = B.inputScreenshot.text.toString()
		
    
		B.media?.moderation = B.inputModeration.text.toString()
		


        val media = B.media ?: return
        val id = media.id ?: return
        mutate(api().editMedia(id, media)) { 
            B.media = it
            loaded()
        }
    }

    override fun create() {
        print("creating media")
        showWait()
        
        B.media = Media()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
            
		B.media?.preview = B.inputPreview.text.toString()
		
    
		B.media?.screenshot = B.inputScreenshot.text.toString()
		
    
		B.media?.moderation = B.inputModeration.text.toString()
		


        val media = B.media ?: return
        mutate(api().createMedia(media)) { 
            B.media = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.media?.id ?: return
        showWait()
        print("deleting media")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().deleteMedia(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

