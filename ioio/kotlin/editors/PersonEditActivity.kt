package mx.ioio.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.EditActivity
import mx.ioio.app.databinding.ActivityPersonEditBinding
import mx.ioio.app.models.Person
import com.google.gson.Gson

class PersonEditActivity : EditActivity() {

    lateinit var B: ActivityPersonEditBinding

    override val TAG = "PersonEditActivity"
    override var model = "person"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = Person::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_person_edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.person)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.person = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().person(id)) {
                B.person = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.person?.let { person ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating person")
        var valid = true

        //Field Validation
            
		//country validation
        val country = B.inputCountry.text.toString()
        if (country.isNullOrBlank()) {
            valid = false
            throwField(B.inputCountry, "EMPTY")
        }
		
    
		//gender validation
        val gender = B.inputGender.text.toString()
        if (gender.isNullOrBlank()) {
            valid = false
            throwField(B.inputGender, "EMPTY")
        }
		
    
		//birthday validation
        val birthday = B.inputBirthday.text.toString()
        if (birthday.isNullOrBlank()) {
            valid = false
            throwField(B.inputBirthday, "EMPTY")
        }
		
    
		//location validation
        val location = B.inputLocation.text.toString()
        if (location.isNullOrBlank()) {
            valid = false
            throwField(B.inputLocation, "EMPTY")
        }
		
    
		//key validation
        val key = B.inputKey.text.toString()
        if (key.isNullOrBlank()) {
            valid = false
            throwField(B.inputKey, "EMPTY")
        }
		
    
		//activity validation
        val activity = B.inputActivity.text.toString()
        if (activity.isNullOrBlank()) {
            valid = false
            throwField(B.inputActivity, "EMPTY")
        }
		


        return valid;
    }

    override fun edit() {
        if (B.person == null) return
        print("editing person")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        

        //Edit Field
            
		B.person?.country = B.inputCountry.text.toString()
		
    
		B.person?.gender = B.inputGender.text.toString()
		
    
		B.person?.birthday = B.inputBirthday.text.toString()
		
    
		B.person?.location = B.inputLocation.text.toString()
		
    
		B.person?.key = B.inputKey.text.toString()
		
    
		B.person?.activity = B.inputActivity.text.toString()
		


        val person = B.person ?: return
        val id = person.id ?: return
        mutate(api().editPerson(id, person)) { 
            B.person = it
            loaded()
        }
    }

    override fun create() {
        print("creating person")
        showWait()
        
        B.person = Person()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
            
		B.person?.country = B.inputCountry.text.toString()
		
    
		B.person?.gender = B.inputGender.text.toString()
		
    
		B.person?.birthday = B.inputBirthday.text.toString()
		
    
		B.person?.location = B.inputLocation.text.toString()
		
    
		B.person?.key = B.inputKey.text.toString()
		
    
		B.person?.activity = B.inputActivity.text.toString()
		


        val person = B.person ?: return
        mutate(api().createPerson(person)) { 
            B.person = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.person?.id ?: return
        showWait()
        print("deleting person")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().deletePerson(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

