package mx.ioio.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.EditActivity
import mx.ioio.app.databinding.ActivityPlaceEditBinding
import mx.ioio.app.models.Place
import com.google.gson.Gson

class PlaceEditActivity : EditActivity() {

    lateinit var B: ActivityPlaceEditBinding

    override val TAG = "PlaceEditActivity"
    override var model = "place"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = Place::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_place_edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.place)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.place = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().place(id)) {
                B.place = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.place?.let { place ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating place")
        var valid = true

        //Field Validation
            
		//country validation
        val country = B.inputCountry.text.toString()
        if (country.isNullOrBlank()) {
            valid = false
            throwField(B.inputCountry, "EMPTY")
        }
		
    
		//lat validation
        val lat = B.inputLat.text.toString()
        if (lat.isNullOrBlank()) {
            valid = false
            throwField(B.inputLat, "EMPTY")
        }
		
    
		//lon validation
        val lon = B.inputLon.text.toString()
        if (lon.isNullOrBlank()) {
            valid = false
            throwField(B.inputLon, "EMPTY")
        }
		
    
		//address validation
        val address = B.inputAddress.text.toString()
        if (address.isNullOrBlank()) {
            valid = false
            throwField(B.inputAddress, "EMPTY")
        }
		
    
		//identifier validation
        val identifier = B.inputIdentifier.text.toString()
        if (identifier.isNullOrBlank()) {
            valid = false
            throwField(B.inputIdentifier, "EMPTY")
        }
		
    
		//state validation
        val state = B.inputState.text.toString()
        if (state.isNullOrBlank()) {
            valid = false
            throwField(B.inputState, "EMPTY")
        }
		


        return valid;
    }

    override fun edit() {
        if (B.place == null) return
        print("editing place")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        

        //Edit Field
            
		B.place?.country = B.inputCountry.text.toString()
		
    
		B.place?.lat = B.inputLat.text.toString()
		
    
		B.place?.lon = B.inputLon.text.toString()
		
    
		B.place?.address = B.inputAddress.text.toString()
		
    
		B.place?.identifier = B.inputIdentifier.text.toString()
		
    
		B.place?.state = B.inputState.text.toString()
		


        val place = B.place ?: return
        val id = place.id ?: return
        mutate(api().editPlace(id, place)) { 
            B.place = it
            loaded()
        }
    }

    override fun create() {
        print("creating place")
        showWait()
        
        B.place = Place()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
            
		B.place?.country = B.inputCountry.text.toString()
		
    
		B.place?.lat = B.inputLat.text.toString()
		
    
		B.place?.lon = B.inputLon.text.toString()
		
    
		B.place?.address = B.inputAddress.text.toString()
		
    
		B.place?.identifier = B.inputIdentifier.text.toString()
		
    
		B.place?.state = B.inputState.text.toString()
		


        val place = B.place ?: return
        mutate(api().createPlace(place)) { 
            B.place = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.place?.id ?: return
        showWait()
        print("deleting place")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().deletePlace(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

