package mx.ioio.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.EditActivity
import mx.ioio.app.databinding.ActivityUserEditBinding
import mx.ioio.app.models.User
import com.google.gson.Gson

class UserEditActivity : EditActivity() {

    lateinit var B: ActivityUserEditBinding

    override val TAG = "UserEditActivity"
    override var model = "user"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = User::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_user_edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.user)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.user = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().user(id)) {
                B.user = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.user?.let { user ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating user")
        var valid = true

        //Field Validation
            
		//first_name validation
        val firstName = B.inputFirstName.text.toString()
        if (firstName.isNullOrBlank()) {
            valid = false
            throwField(B.inputFirstName, "EMPTY")
        }
		
    
		//last_name validation
        val lastName = B.inputLastName.text.toString()
        if (lastName.isNullOrBlank()) {
            valid = false
            throwField(B.inputLastName, "EMPTY")
        }
		
    
		//email validation
        val email = B.inputEmail.text.toString()
        if (email.isNullOrBlank()) {
            valid = false
            throwField(B.inputEmail, "EMPTY")
        }
		


        return valid;
    }

    override fun edit() {
        if (B.user == null) return
        print("editing user")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        

        //Edit Field
            
		B.user?.firstName = B.inputFirstName.text.toString()
		
    
		B.user?.lastName = B.inputLastName.text.toString()
		
    
		B.user?.email = B.inputEmail.text.toString()
		


        val user = B.user ?: return
        val id = user.id ?: return
        mutate(api().editUser(id, user)) { 
            B.user = it
            loaded()
        }
    }

    override fun create() {
        print("creating user")
        showWait()
        
        B.user = User()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
            
		B.user?.firstName = B.inputFirstName.text.toString()
		
    
		B.user?.lastName = B.inputLastName.text.toString()
		
    
		B.user?.email = B.inputEmail.text.toString()
		


        val user = B.user ?: return
        mutate(api().createUser(user)) { 
            B.user = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.user?.id ?: return
        showWait()
        print("deleting user")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().deleteUser(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

