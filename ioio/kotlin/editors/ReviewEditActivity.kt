package mx.ioio.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.EditActivity
import mx.ioio.app.databinding.ActivityReviewEditBinding
import mx.ioio.app.models.Review
import com.google.gson.Gson

class ReviewEditActivity : EditActivity() {

    lateinit var B: ActivityReviewEditBinding

    override val TAG = "ReviewEditActivity"
    override var model = "review"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = Review::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_review_edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.review)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.review = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().review(id)) {
                B.review = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.review?.let { review ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating review")
        var valid = true

        //Field Validation
            
		//reviewer_id validation
        val reviewerId = B.inputReviewerId.text.toString()
        if (reviewerId.isNullOrBlank()) {
            valid = false
            throwField(B.inputReviewerId, "EMPTY")
        }
		
    
		//target validation
        val target = B.inputTarget.text.toString()
        if (target.isNullOrBlank()) {
            valid = false
            throwField(B.inputTarget, "EMPTY")
        }
		
    
		//comment validation
        val comment = B.inputComment.text.toString()
        if (comment.isNullOrBlank()) {
            valid = false
            throwField(B.inputComment, "EMPTY")
        }
		
    
		//rate validation
        val rate = B.inputRate.text.toString()
        if (rate.isNullOrBlank()) {
            valid = false
            throwField(B.inputRate, "EMPTY")
        }
		
    
		//moderation validation
        val moderation = B.inputModeration.text.toString()
        if (moderation.isNullOrBlank()) {
            valid = false
            throwField(B.inputModeration, "EMPTY")
        }
		
    
		//reviewed validation
        val reviewed = B.inputReviewed.text.toString()
        if (reviewed.isNullOrBlank()) {
            valid = false
            throwField(B.inputReviewed, "EMPTY")
        }
		


        return valid;
    }

    override fun edit() {
        if (B.review == null) return
        print("editing review")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        

        //Edit Field
            
		B.review?.reviewerId = B.inputReviewerId.text.toString().toIntOrNull()
		
    
		B.review?.target = B.inputTarget.text.toString()
		
    
		B.review?.comment = B.inputComment.text.toString()
		
    
		B.review?.rate = B.inputRate.text.toString().toIntOrNull()
		
    
		B.review?.moderation = B.inputModeration.text.toString()
		
    
		B.review?.reviewed = B.inputReviewed.text.toString()
		


        val review = B.review ?: return
        val id = review.id ?: return
        mutate(api().editReview(id, review)) { 
            B.review = it
            loaded()
        }
    }

    override fun create() {
        print("creating review")
        showWait()
        
        B.review = Review()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
            
		B.review?.reviewerId = B.inputReviewerId.text.toString().toIntOrNull()
		
    
		B.review?.target = B.inputTarget.text.toString()
		
    
		B.review?.comment = B.inputComment.text.toString()
		
    
		B.review?.rate = B.inputRate.text.toString().toIntOrNull()
		
    
		B.review?.moderation = B.inputModeration.text.toString()
		
    
		B.review?.reviewed = B.inputReviewed.text.toString()
		


        val review = B.review ?: return
        mutate(api().createReview(review)) { 
            B.review = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.review?.id ?: return
        showWait()
        print("deleting review")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().deleteReview(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

