package mx.ioio.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.EditActivity
import mx.ioio.app.databinding.ActivityItemEditBinding
import mx.ioio.app.models.Item
import com.google.gson.Gson

class ItemEditActivity : EditActivity() {

    lateinit var B: ActivityItemEditBinding

    override val TAG = "ItemEditActivity"
    override var model = "item"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = Item::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_item_edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.item)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.item = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().item(id)) {
                B.item = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.item?.let { item ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating item")
        var valid = true

        //Field Validation
            
		//media validation
        val media = B.inputMedia.text.toString()
        if (media.isNullOrBlank()) {
            valid = false
            throwField(B.inputMedia, "EMPTY")
        }
		
    
		//owner_id validation
        val ownerId = B.inputOwnerId.text.toString()
        if (ownerId.isNullOrBlank()) {
            valid = false
            throwField(B.inputOwnerId, "EMPTY")
        }
		
    
		//category_id validation
        val categoryId = B.inputCategoryId.text.toString()
        if (categoryId.isNullOrBlank()) {
            valid = false
            throwField(B.inputCategoryId, "EMPTY")
        }
		
    
		//price validation
        val price = B.inputPrice.text.toString()
        if (price.isNullOrBlank()) {
            valid = false
            throwField(B.inputPrice, "EMPTY")
        }
		
    
		//value validation
        val value = B.inputValue.text.toString()
        if (value.isNullOrBlank()) {
            valid = false
            throwField(B.inputValue, "EMPTY")
        }
		
    
		//valuation validation
        val valuation = B.inputValuation.text.toString()
        if (valuation.isNullOrBlank()) {
            valid = false
            throwField(B.inputValuation, "EMPTY")
        }
		
    
		//currency validation
        val currency = B.inputCurrency.text.toString()
        if (currency.isNullOrBlank()) {
            valid = false
            throwField(B.inputCurrency, "EMPTY")
        }
		
    
		//frame validation
        val frame = B.inputFrame.text.toString()
        if (frame.isNullOrBlank()) {
            valid = false
            throwField(B.inputFrame, "EMPTY")
        }
		
    
		//audience validation
        val audience = B.inputAudience.text.toString()
        if (audience.isNullOrBlank()) {
            valid = false
            throwField(B.inputAudience, "EMPTY")
        }
		
    
		//outreach validation
        val outreach = B.inputOutreach.text.toString()
        if (outreach.isNullOrBlank()) {
            valid = false
            throwField(B.inputOutreach, "EMPTY")
        }
		
    
		//kind validation
        val kind = B.inputKind.text.toString()
        if (kind.isNullOrBlank()) {
            valid = false
            throwField(B.inputKind, "EMPTY")
        }
		
    
		//location validation
        val location = B.inputLocation.text.toString()
        if (location.isNullOrBlank()) {
            valid = false
            throwField(B.inputLocation, "EMPTY")
        }
		
    
		//keywords validation
        val keywords = B.inputKeywords.text.toString()
        if (keywords.isNullOrBlank()) {
            valid = false
            throwField(B.inputKeywords, "EMPTY")
        }
		
    
		//brand validation
        val brand = B.inputBrand.text.toString()
        if (brand.isNullOrBlank()) {
            valid = false
            throwField(B.inputBrand, "EMPTY")
        }
		


        return valid;
    }

    override fun edit() {
        if (B.item == null) return
        print("editing item")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        

        //Edit Field
            
		B.item?.media = B.inputMedia.text.toString()
		
    
		B.item?.ownerId = B.inputOwnerId.text.toString().toIntOrNull()
		
    
		B.item?.categoryId = B.inputCategoryId.text.toString().toIntOrNull()
		
    
		B.item?.price = B.inputPrice.text.toString()
		
    
		B.item?.value = B.inputValue.text.toString()
		
    
		B.item?.valuation = B.inputValuation.text.toString()
		
    
		B.item?.currency = B.inputCurrency.text.toString()
		
    
		B.item?.frame = B.inputFrame.text.toString()
		
    
		B.item?.audience = B.inputAudience.text.toString()
		
    
		B.item?.outreach = B.inputOutreach.text.toString()
		
    
		B.item?.kind = B.inputKind.text.toString()
		
    
		B.item?.location = B.inputLocation.text.toString()
		
    
		B.item?.keywords = B.inputKeywords.text.toString()
		
    
		B.item?.brand = B.inputBrand.text.toString()
		


        val item = B.item ?: return
        val id = item.id ?: return
        mutate(api().editItem(id, item)) { 
            B.item = it
            loaded()
        }
    }

    override fun create() {
        print("creating item")
        showWait()
        
        B.item = Item()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
            
		B.item?.media = B.inputMedia.text.toString()
		
    
		B.item?.ownerId = B.inputOwnerId.text.toString().toIntOrNull()
		
    
		B.item?.categoryId = B.inputCategoryId.text.toString().toIntOrNull()
		
    
		B.item?.price = B.inputPrice.text.toString()
		
    
		B.item?.value = B.inputValue.text.toString()
		
    
		B.item?.valuation = B.inputValuation.text.toString()
		
    
		B.item?.currency = B.inputCurrency.text.toString()
		
    
		B.item?.frame = B.inputFrame.text.toString()
		
    
		B.item?.audience = B.inputAudience.text.toString()
		
    
		B.item?.outreach = B.inputOutreach.text.toString()
		
    
		B.item?.kind = B.inputKind.text.toString()
		
    
		B.item?.location = B.inputLocation.text.toString()
		
    
		B.item?.keywords = B.inputKeywords.text.toString()
		
    
		B.item?.brand = B.inputBrand.text.toString()
		


        val item = B.item ?: return
        mutate(api().createItem(item)) { 
            B.item = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.item?.id ?: return
        showWait()
        print("deleting item")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().deleteItem(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

