package mx.ioio.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.EditActivity
import mx.ioio.app.databinding.ActivityAgreementEditBinding
import mx.ioio.app.models.Agreement
import com.google.gson.Gson

class AgreementEditActivity : EditActivity() {

    lateinit var B: ActivityAgreementEditBinding

    override val TAG = "AgreementEditActivity"
    override var model = "agreement"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = Agreement::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_agreement_edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.agreement)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.agreement = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().agreement(id)) {
                B.agreement = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.agreement?.let { agreement ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating agreement")
        var valid = true

        //Field Validation
            
		//kind_id validation
        val kindId = B.inputKindId.text.toString()
        if (kindId.isNullOrBlank()) {
            valid = false
            throwField(B.inputKindId, "EMPTY")
        }
		
    
		//hash validation
        val hash = B.inputHash.text.toString()
        if (hash.isNullOrBlank()) {
            valid = false
            throwField(B.inputHash, "EMPTY")
        }
		
    
		//version validation
        val version = B.inputVersion.text.toString()
        if (version.isNullOrBlank()) {
            valid = false
            throwField(B.inputVersion, "EMPTY")
        }
		


        return valid;
    }

    override fun edit() {
        if (B.agreement == null) return
        print("editing agreement")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        undefined
        undefined
        undefined
        undefined

        //Edit Field
            
		B.agreement?.kindId = B.inputKindId.text.toString().toIntOrNull()
		
    
		B.agreement?.hash = B.inputHash.text.toString()
		
    
		B.agreement?.version = B.inputVersion.text.toString().toIntOrNull()
		


        val agreement = B.agreement ?: return
        val id = agreement.id ?: return
        mutate(api().editAgreement(id, agreement)) { 
            B.agreement = it
            loaded()
        }
    }

    override fun create() {
        print("creating agreement")
        showWait()
        
        B.agreement = Agreement()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
            
		B.agreement?.kindId = B.inputKindId.text.toString().toIntOrNull()
		
    
		B.agreement?.hash = B.inputHash.text.toString()
		
    
		B.agreement?.version = B.inputVersion.text.toString().toIntOrNull()
		


        val agreement = B.agreement ?: return
        mutate(api().createAgreement(agreement)) { 
            B.agreement = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.agreement?.id ?: return
        showWait()
        print("deleting agreement")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().deleteAgreement(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

