package mx.ioio.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.EditActivity
import mx.ioio.app.databinding.ActivityOperationEditBinding
import mx.ioio.app.models.Operation
import com.google.gson.Gson

class OperationEditActivity : EditActivity() {

    lateinit var B: ActivityOperationEditBinding

    override val TAG = "OperationEditActivity"
    override var model = "operation"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = Operation::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_operation_edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.operation)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.operation = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().operation(id)) {
                B.operation = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.operation?.let { operation ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating operation")
        var valid = true

        //Field Validation
            
		//landlord_id validation
        val landlordId = B.inputLandlordId.text.toString()
        if (landlordId.isNullOrBlank()) {
            valid = false
            throwField(B.inputLandlordId, "EMPTY")
        }
		
    
		//tenant_id validation
        val tenantId = B.inputTenantId.text.toString()
        if (tenantId.isNullOrBlank()) {
            valid = false
            throwField(B.inputTenantId, "EMPTY")
        }
		
    
		//item_id validation
        val itemId = B.inputItemId.text.toString()
        if (itemId.isNullOrBlank()) {
            valid = false
            throwField(B.inputItemId, "EMPTY")
        }
		
    
		//agreement_id validation
        val agreementId = B.inputAgreementId.text.toString()
        if (agreementId.isNullOrBlank()) {
            valid = false
            throwField(B.inputAgreementId, "EMPTY")
        }
		
    
		//nda_id validation
        val ndaId = B.inputNdaId.text.toString()
        if (ndaId.isNullOrBlank()) {
            valid = false
            throwField(B.inputNdaId, "EMPTY")
        }
		
    
		//place_id validation
        val placeId = B.inputPlaceId.text.toString()
        if (placeId.isNullOrBlank()) {
            valid = false
            throwField(B.inputPlaceId, "EMPTY")
        }
		
    
		//kind validation
        val kind = B.inputKind.text.toString()
        if (kind.isNullOrBlank()) {
            valid = false
            throwField(B.inputKind, "EMPTY")
        }
		
    
		//iterations validation
        val iterations = B.inputIterations.text.toString()
        if (iterations.isNullOrBlank()) {
            valid = false
            throwField(B.inputIterations, "EMPTY")
        }
		
    
		//iteration validation
        val iteration = B.inputIteration.text.toString()
        if (iteration.isNullOrBlank()) {
            valid = false
            throwField(B.inputIteration, "EMPTY")
        }
		
    
		//transfer validation
        val transfer = B.inputTransfer.text.toString()
        if (transfer.isNullOrBlank()) {
            valid = false
            throwField(B.inputTransfer, "EMPTY")
        }
		
    
		//release validation
        val release = B.inputRelease.text.toString()
        if (release.isNullOrBlank()) {
            valid = false
            throwField(B.inputRelease, "EMPTY")
        }
		


        return valid;
    }

    override fun edit() {
        if (B.operation == null) return
        print("editing operation")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        undefined
        undefined
        undefined
        undefined

        //Edit Field
            
		B.operation?.landlordId = B.inputLandlordId.text.toString().toIntOrNull()
		
    
		B.operation?.tenantId = B.inputTenantId.text.toString().toIntOrNull()
		
    
		B.operation?.itemId = B.inputItemId.text.toString().toIntOrNull()
		
    
		B.operation?.agreementId = B.inputAgreementId.text.toString().toIntOrNull()
		
    
		B.operation?.ndaId = B.inputNdaId.text.toString().toIntOrNull()
		
    
		B.operation?.placeId = B.inputPlaceId.text.toString().toIntOrNull()
		
    
		B.operation?.kind = B.inputKind.text.toString()
		
    
		B.operation?.iterations = B.inputIterations.text.toString().toIntOrNull()
		
    
		B.operation?.iteration = B.inputIteration.text.toString().toIntOrNull()
		
    
		B.operation?.transfer = B.inputTransfer.text.toString()
		
    
		B.operation?.release = B.inputRelease.text.toString()
		


        val operation = B.operation ?: return
        val id = operation.id ?: return
        mutate(api().editOperation(id, operation)) { 
            B.operation = it
            loaded()
        }
    }

    override fun create() {
        print("creating operation")
        showWait()
        
        B.operation = Operation()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
            
		B.operation?.landlordId = B.inputLandlordId.text.toString().toIntOrNull()
		
    
		B.operation?.tenantId = B.inputTenantId.text.toString().toIntOrNull()
		
    
		B.operation?.itemId = B.inputItemId.text.toString().toIntOrNull()
		
    
		B.operation?.agreementId = B.inputAgreementId.text.toString().toIntOrNull()
		
    
		B.operation?.ndaId = B.inputNdaId.text.toString().toIntOrNull()
		
    
		B.operation?.placeId = B.inputPlaceId.text.toString().toIntOrNull()
		
    
		B.operation?.kind = B.inputKind.text.toString()
		
    
		B.operation?.iterations = B.inputIterations.text.toString().toIntOrNull()
		
    
		B.operation?.iteration = B.inputIteration.text.toString().toIntOrNull()
		
    
		B.operation?.transfer = B.inputTransfer.text.toString()
		
    
		B.operation?.release = B.inputRelease.text.toString()
		


        val operation = B.operation ?: return
        mutate(api().createOperation(operation)) { 
            B.operation = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.operation?.id ?: return
        showWait()
        print("deleting operation")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().deleteOperation(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

