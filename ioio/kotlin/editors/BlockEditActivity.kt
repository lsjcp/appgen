package mx.ioio.app.editors

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import mx.ioio.app.R
import mx.ioio.app.common.EditActivity
import mx.ioio.app.databinding.ActivityBlockEditBinding
import mx.ioio.app.models.Block
import com.google.gson.Gson

class BlockEditActivity : EditActivity() {

    lateinit var B: ActivityBlockEditBinding

    override val TAG = "BlockEditActivity"
    override var model = "block"
    
    override fun editable(): Boolean = true
    override fun creatable(): Boolean = true
    override fun deletable(): Boolean = true

    val className = Block::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        B = bind(R.layout.activity_block_edit)
        wait = B.waitScreen
        backToolbar(B.include.toolbar, R.string.block)
        load()
    }

    override fun load() {
        showWait()
        intent?.getStringExtra(item_json)?.let { json ->
            B.block = Gson().fromJson(json, className)
            loaded()
        }
        intent?.getIntExtra(item_id, -1)?.let { 
            if (it == -1) return
            refresh()
        }
        
    }

    override fun refresh() {
        id?.let { id ->
            query(api().block(id)) {
                B.block = it
                loaded()
            }
        }
    }

    override fun loaded() {
        hideWait()
        B.block?.let { block ->
            //TODO: After load actionns
        }
    }

    override fun validate(): Boolean {
        print("validating block")
        var valid = true

        //Field Validation
            
		//items validation
        val items = B.inputItems.text.toString()
        if (items.isNullOrBlank()) {
            valid = false
            throwField(B.inputItems, "EMPTY")
        }
		
    
		//contentType validation
        val contentType = B.inputContentType.text.toString()
        if (contentType.isNullOrBlank()) {
            valid = false
            throwField(B.inputContentType, "EMPTY")
        }
		
    
		//itemLayout validation
        val itemLayout = B.inputItemLayout.text.toString()
        if (itemLayout.isNullOrBlank()) {
            valid = false
            throwField(B.inputItemLayout, "EMPTY")
        }
		
    
		//groupLayout validation
        val groupLayout = B.inputGroupLayout.text.toString()
        if (groupLayout.isNullOrBlank()) {
            valid = false
            throwField(B.inputGroupLayout, "EMPTY")
        }
		


        return valid;
    }

    override fun edit() {
        if (B.block == null) return
        print("editing block")
        showWait()
        

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //PROTO
        undefined
        undefined
        undefined
        undefined

        //Edit Field
            
		B.block?.items = B.inputItems.text.toString()
		
    
		B.block?.contentType = B.inputContentType.text.toString()
		
    
		B.block?.itemLayout = B.inputItemLayout.text.toString()
		
    
		B.block?.groupLayout = B.inputGroupLayout.text.toString()
		


        val block = B.block ?: return
        val id = block.id ?: return
        mutate(api().editBlock(id, block)) { 
            B.block = it
            loaded()
        }
    }

    override fun create() {
        print("creating block")
        showWait()
        
        B.block = Block()

        if (!validate()) {
            hideWait()
            genericError(R.string.error_non_valid)
            return
        }

        //Edit Field
            
		B.block?.items = B.inputItems.text.toString()
		
    
		B.block?.contentType = B.inputContentType.text.toString()
		
    
		B.block?.itemLayout = B.inputItemLayout.text.toString()
		
    
		B.block?.groupLayout = B.inputGroupLayout.text.toString()
		


        val block = B.block ?: return
        mutate(api().createBlock(block)) { 
            B.block = it
            loaded()
        }
    }

    override fun delete() {
        val id = B.block?.id ?: return
        showWait()
        print("deleting block")

        confirmDestroy(getString(R.string.confirm_destroy)) {
            mutate(api().deleteBlock(id)) { 
                showDone {
                    finish()
                }
            }
        }
    }
}

