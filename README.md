##APPGEN

A small Node library to generate Java Android and iOS Swift Code from REST APIs

##NODE DEPENDENCIES
mkdirp

##USAGE
node main.js config

## License

GPLv3
